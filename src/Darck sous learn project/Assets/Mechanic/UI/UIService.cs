﻿using Infrastructure;
using Service.Input;
using UnityEngine;
using Mechanic.Health.UI;

namespace Mechanic.UI
{
	public class UIService : MonoBehaviour, IUIService, IInitializer
	{
		[SerializeField] private Canvas _rootCanvasPrefab;
		[SerializeField] private Canvas _healthBarPrefab;
		[SerializeField] private Transform _uiRoot;
		
		private Canvas _rootCanvas;
		private Health.Health _playerHealth;

		public void Construct(Health.Health playerHealth)
		{
			_playerHealth = playerHealth;
		}
		
		public void Initialize()
		{
			_rootCanvas = Instantiate(_rootCanvasPrefab, _uiRoot);
			Instantiate(_healthBarPrefab, _rootCanvas.transform)
				.GetComponentInChildren<HealthBar>()
				.Construct(_playerHealth);
		}
	}
}