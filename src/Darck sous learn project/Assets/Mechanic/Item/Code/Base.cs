using UnityEngine;

namespace Mechanic.Item
{
  public class Base : ScriptableObject
  {
    [SerializeField] private string _name;
    [SerializeField] private Sprite _icon;
  }
}