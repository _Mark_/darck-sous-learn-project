﻿using Infrastructure;
using UnityEngine;

namespace Mechanic.Item
{
  [CreateAssetMenu(fileName = "Weapon", menuName = Const.CreateAssetMenu.MenuName.ITEM_WEAPON)]
  public class Weapon : Base
  {
    [SerializeField] private GameObject _model;

    public GameObject Model => _model;
  }
}