﻿using System;
using UnityEngine;

namespace Mechanic.Interaction
{
	public class Trigger : MonoBehaviour
	{
		public event Action<Collider> ColliderStay;
		public event Action<Collider> ColliderExited;
		public event Action<Collider> ColliderEntered;
		
		private void OnTriggerEnter(Collider other)
		{
			ColliderEntered?.Invoke(other);
		}

		private void OnTriggerStay(Collider other)
		{
			ColliderStay?.Invoke(other);
		}

		private void OnTriggerExit(Collider other)
		{
			ColliderExited?.Invoke(other);
		}
	}
}