﻿using System;
using UnityEngine;

namespace Mechanic.WeaponSlot
{
	public class WeaponSlotController : MonoBehaviour
	{
		[SerializeField] private WeaponHolderSlot _leftWeaponHolderSlot;
		[SerializeField] private WeaponHolderSlot _rightWeaponHolderSlot;

		public WeaponHolderSlot Slot(SlotSide side)
		{
			switch (side)
			{
				case SlotSide.Left:
					return _leftWeaponHolderSlot;

				case SlotSide.Right:
					return _rightWeaponHolderSlot;

				default:
					Debug.Log($"Incorrect side: \"{side}\"");
					break;
			}

			return _leftWeaponHolderSlot;
		}

		public void LoadWeapon(Item.Weapon weapon, SlotSide side)
		{
			switch (side)
			{
				case SlotSide.Left:
					LoadWeaponToSlot(_leftWeaponHolderSlot, weapon);
					break;

				case SlotSide.Right:
					LoadWeaponToSlot(_rightWeaponHolderSlot, weapon);
					break;

				default:
					Debug.Log($"Incorrect side: \"{side}\"");
					break;
			}
		}

		private void LoadWeaponToSlot(WeaponHolderSlot slot, Item.Weapon weapon)
		{
			if (slot.IsHasWeapon)
				slot.DestroyWeapon();

			slot.LoadWeaponModel(weapon);
		}
	}
}