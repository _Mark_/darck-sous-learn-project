﻿using UnityEngine;

namespace Mechanic.WeaponSlot
{
  public class WeaponHolderSlot : MonoBehaviour
  {
    [SerializeField] private Transform _parentForWeapon;

    private GameObject _weaponModel;

    public bool IsHasWeapon =>
      _weaponModel != null;

    private void Awake()
    {
      if (_parentForWeapon == null)
      {
        Debug.LogError($"NullReferenceException: {nameof(_parentForWeapon)}");
      }
    }

    public void UnloadWeapon()
    {
      if (IsHasWeapon)
      {
        _weaponModel.SetActive(false);
      }
    }

    public void DestroyWeapon()
    {
      if (IsHasWeapon == false)
        return;
      
      Destroy(_weaponModel);
    }

    public void LoadWeaponModel(Item.Weapon weapon)
    {
      if (weapon == null) // Maybe it is not necessary?
      {
        UnloadWeapon();
        return;
      }
      
      if (weapon.Model == null)
      {
        Debug.LogError("Model weapon is null!");
        return;
      }

      _weaponModel = Instantiate(weapon.Model, _parentForWeapon.position, _parentForWeapon.rotation, _parentForWeapon);
    }
  }
}