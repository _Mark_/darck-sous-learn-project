﻿using Mechanic.Player.Locomotion.Mode;

namespace Mechanic.Player.Locomotion
{
	public class ModeSwitcher : ILocomotionModeSwitcher
	{
		private ILocomotion _currentLocomotion;

		private readonly PlayerLocomotion _playerLocomotion;
		private readonly TransitionManager _transitionManager;
		private readonly ModeCollection _modeCollection;

		public ModeSwitcher(
			PlayerLocomotion playerLocomotion,
			TransitionManager transitionManager,
			ModeCollection modeCollection)
		{
			_playerLocomotion = playerLocomotion;
			_transitionManager = transitionManager;
			_modeCollection = modeCollection;
		}

		public bool IsHasUpdateLocomotion { get; private set; }
		public ILocomotionUpdate ActiveUpdateLocomotion { get; private set; }

		public void SetStartMode(EMode mode) =>
			StartMode(mode);

		public void SwitchToMode(EMode mode)
		{
			_currentLocomotion.Stop();
			StartMode(mode);
		}

		private void StartMode(EMode mode)
		{
			SetNewCurrentMode(mode);
			ChangeModeUpdate(mode);
		}

		private void SetNewCurrentMode(EMode mode)
		{
			_currentLocomotion = _modeCollection.LocomotionModes[mode];
			_currentLocomotion.Start();
			_transitionManager.SwitchTransitions(_currentLocomotion.Transitions);
			_playerLocomotion.CurrentModeIndicator = mode;
		}

		private void ChangeModeUpdate(EMode mode)
		{
			if (_modeCollection.LocomotionUpdates.ContainsKey(mode))
			{
				IsHasUpdateLocomotion = true;
				ActiveUpdateLocomotion =
					_modeCollection.LocomotionUpdates[mode];
			}
			else
				IsHasUpdateLocomotion = false;
		}
	}
}