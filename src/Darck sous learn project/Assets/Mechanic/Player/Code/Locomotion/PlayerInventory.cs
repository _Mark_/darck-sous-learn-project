﻿using Mechanic.WeaponSlot;
using UnityEngine;

namespace Mechanic.Player.Locomotion
{
  public class PlayerInventory : MonoBehaviour
  {
    [SerializeField] private Item.Weapon _weapon;
    [SerializeField] private WeaponSlotController _weaponSlotController;

    public WeaponSlotController HandsSlots => _weaponSlotController;

    public void Start()
    {
      _weaponSlotController.LoadWeapon(_weapon, SlotSide.Right);
    }
  }
}