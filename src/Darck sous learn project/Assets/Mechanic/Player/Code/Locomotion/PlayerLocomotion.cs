using System.Collections.Generic;
using Infrastructure;
using Infrastructure.DI;
using Mechanic.Player.AnimationControl;
using Mechanic.Player.Locomotion.Mode;
using Service.Input;
using UnityEngine;
using Settings = Mechanic.Player.Locomotion.PlayerLocomotionSettings;
using Transform = UnityEngine.Transform;
using FallHandler = Mechanic.Player.Locomotion.Handler.Fall;

namespace Mechanic.Player.Locomotion
{
	[RequireComponent(typeof(Rigidbody), typeof(CapsuleCollider))]
	public sealed class PlayerLocomotion : MonoBehaviour, IInitializer
	{
		private const EMode START_LOCOMOTION_MODE = EMode.Walking;

		[SerializeField] private EMode _currentModeIndicator;
		[Space]
		[SerializeField] private Transform _camera;

		[SerializeField] private Settings _playerLocomotionSettings;
		[Space]
		[SerializeField] private FallHandler _fallHandler;
		[SerializeField] private GroundDetector _groundDetector;
		[SerializeField] private RotationHandler _rotationHandler;

		private Landing _landing;
		private ModeSwitcher _modeSwitcher;
		// private IInputService _inputService;
		private ModeCollection _modeCollection;
		private TransitionManager _transitionManager;
		private DependencyCollection _dependencyCollection;

		private readonly List<IOnDestroy> _destructionList = new List<IOnDestroy>();

		public EMode CurrentModeIndicator
		{
			set => _currentModeIndicator = value;
		}

		private void OnDestroy()
		{
			foreach (var destruction in _destructionList)
				destruction.OnDestroy();
		}

		private void FixedUpdate()
		{
			var delta = Time.deltaTime;
			_groundDetector.Handle();

			UpdateLocomotion(delta);
			_transitionManager.Update(delta);
		}

		public void Construct(
			IInputService inputService,
			PlayerAnimationHandler playerAnimationHandler)
		{
			var dependencies = ModeCollectionDependencies(inputService, playerAnimationHandler);
			
			_transitionManager = new TransitionManager();
			_modeCollection = new ModeCollection(dependencies);			
			CreateLocomotionModeSwitcher();
			ConstructSharedHandlers(dependencies);
		}

		public void Initialize()
		{
			_modeSwitcher.SetStartMode(START_LOCOMOTION_MODE);
		}

		private void CreateLocomotionModeSwitcher()
		{
			_modeSwitcher = new ModeSwitcher(
				this,
				_transitionManager,
				_modeCollection);
		}

		private void ConstructSharedHandlers(Dependencies dependencies)
		{
			_fallHandler.Construct(dependencies);
			_groundDetector.Construct(dependencies, transform, _capsuleCollider);
			_rotationHandler.Construct(transform, _camera, _inputService, true);
		}

		private void UpdateLocomotion(float delta)
		{
			if (_modeSwitcher.IsHasUpdateLocomotion)
				_modeSwitcher.ActiveUpdateLocomotion.Update(delta);
		}

		private Dependencies ModeCollectionDependencies(
			IInputService inputService,
			PlayerAnimationHandler playerAnimationHandler)
		{
			var rigidBody = GetComponent<Rigidbody>();
			var capsuleCollider = GetComponent<CapsuleCollider>();
			
			var dependencies = new Dependencies(
				_camera,
				transform,
				rigidBody,
				_fallHandler,
				inputService,
				_groundDetector,
				_rotationHandler,
				capsuleCollider,
				_destructionList,
				playerAnimationHandler,
				_modeSwitcher,
				_playerLocomotionSettings);

			_dependencyCollection = new DependencyCollection();
			_dependencyCollection.Add<>().WithKey().Instance().As();
			_dependencyCollection.Add<>().Instance().As();
			_dependencyCollection.Add<>()
			
			
			
			
			return dependencies;
		}
	}
}