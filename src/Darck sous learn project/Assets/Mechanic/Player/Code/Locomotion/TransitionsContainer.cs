﻿namespace Mechanic.Player.Locomotion
{
	public class TransitionsContainer
	{
		private readonly ITransition[] _transitions;
		private readonly ITransitionUpdate[] _transitionUpdates;
		
		public TransitionsContainer(ITransition[] transitions, ITransitionUpdate[] transitionUpdates)
		{
			_transitions = transitions;
			_transitionUpdates = transitionUpdates;
		}

		public ITransition[] Transitions => _transitions;
		public ITransitionUpdate[] TransitionUpdates => _transitionUpdates;
	}
}