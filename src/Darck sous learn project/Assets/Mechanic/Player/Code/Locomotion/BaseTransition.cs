﻿namespace Mechanic.Player.Locomotion
{
	public abstract class BaseTransition
	{
		private readonly ILocomotionModeSwitcher _locomotionModeSwitcher;

		protected abstract EMode LocomotionTarget { get; }

		protected BaseTransition(ILocomotionModeSwitcher locomotionModeSwitcher)
		{
			_locomotionModeSwitcher = locomotionModeSwitcher;
		}

		public void MakeTransition() => 
			_locomotionModeSwitcher.SwitchToMode(LocomotionTarget);
	}
}