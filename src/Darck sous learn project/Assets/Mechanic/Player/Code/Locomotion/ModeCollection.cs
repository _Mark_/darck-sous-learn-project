﻿using System.Collections.Generic;
using Mechanic.Player.Locomotion.Mode;
using FallHandler = Mechanic.Player.Locomotion.Handler.Fall;

namespace Mechanic.Player.Locomotion
{
	public sealed class ModeCollection
	{
		private readonly Dictionary<EMode, ILocomotion> _locomotionModes =
			new Dictionary<EMode, ILocomotion>();
		private readonly Dictionary<EMode, ILocomotionUpdate> _locomotionUpdates =
			new Dictionary<EMode, ILocomotionUpdate>();

		public ModeCollection(Dependencies dependencies) => 
			CreateLocomotionModes(dependencies);

		public IReadOnlyDictionary<EMode, ILocomotion> LocomotionModes =>
			_locomotionModes;
		public IReadOnlyDictionary<EMode, ILocomotionUpdate> LocomotionUpdates =>
			_locomotionUpdates;

		private void CreateLocomotionModes(Dependencies dependencies)
		{
			var locomotionModeRegistrar = new LocomotionModeRegistrar(
				dependencies,
				_locomotionModes,
				_locomotionUpdates);
			locomotionModeRegistrar.Register();
		}
	}
}