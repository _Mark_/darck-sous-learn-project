﻿using System.Collections.Generic;
using Mechanic.Player.AnimationControl;
using Service.Input;
using UnityEngine;
using Fall = Mechanic.Player.Locomotion.Mode.Fall;
using FallHandler = Mechanic.Player.Locomotion.Handler.Fall;
using GetHit = Mechanic.Player.Locomotion.Mode.GetHit;
using Landing = Mechanic.Player.Locomotion.Mode.Landing;
using Roll = Mechanic.Player.Locomotion.Mode.Roll;
using Walking = Mechanic.Player.Locomotion.Mode.Walking;
using Extension;
using Infrastructure;
using Mechanic.Player.Locomotion.Mode;

namespace Mechanic.Player.Locomotion
{
	public class LocomotionModeRegistrar
	{
		private Fall _fall;
		private Roll _roll;
		private GetHit _getHit;
		private Landing _landing;
		private Walking _walking;

		private readonly Dependencies _dependencies;
		private readonly Dictionary<EMode, ILocomotion> _locomotionModes;
		private readonly Dictionary<EMode, ILocomotionUpdate> _locomotionUpdates;

		public LocomotionModeRegistrar(
			Dependencies dependencies,
			Dictionary<EMode, ILocomotion> locomotionModes,
			Dictionary<EMode, ILocomotionUpdate> locomotionUpdates)
		{
			_dependencies = dependencies;
			_locomotionModes = locomotionModes;
			_locomotionUpdates = locomotionUpdates;
		}

		public Walking Walking => _walking;

		public void Register()
		{
			CreateModes();
			FillLocomotionModeDictionary();
			FillLocomotionModeUpdatesDictionary();
			FillDestroyedList();
		}

		private void FillLocomotionModeUpdatesDictionary()
		{
			foreach (var pair in _locomotionModes)
			{
				var mode = pair.Key;
				var locomotionMode = pair.Value;
				if (locomotionMode is ILocomotionUpdate locomotionModeUpdate)
					_locomotionUpdates.Add(mode, locomotionModeUpdate);
			}
		}

		private void FillDestroyedList()
		{
			foreach (var pair in _locomotionModes)
			{
				var locomotionMode = pair.Value;
				if (locomotionMode is IOnDestroy destruction)
					_dependencies.DestructionList.Add(destruction);
			}
		}

		private void CreateModes()
		{
			_fall = CreateFallMode();
			_roll = CreateRollMode();
			_getHit = CreateGetHitMode();
			_landing = CreateLandingMode();
			_walking = CreateWalkingMode();
		}

		private void FillLocomotionModeDictionary()
		{
			_locomotionModes
				.AddNew(EMode.Roll, _roll)
				.AddNew(EMode.Fall, _fall)
				.AddNew(EMode.GetHit, _getHit)
				.AddNew(EMode.Landing, _landing)
				.AddNew(EMode.Walking, _walking);
		}

		private Fall CreateFallMode() =>
			new Fall(
				_playerLocomotionSettings.FallSettings,
				_camera,
				_transform,
				_rigidbody,
				_fallHandler,
				_inputService,
				_groundDetector,
				_switchAnimationMode,
				_locomotionModeSwitcher);

		private Roll CreateRollMode() =>
			new Roll(
				_camera,
				_transform,
				_rigidbody,
				_fallHandler,
				_inputService,
				_groundDetector,
				_rotationHandler,
				_rootMotionContainer,
				_switchAnimationMode,
				_playerAnimationHandler,
				_locomotionModeSwitcher);

		private GetHit CreateGetHitMode() =>
			new GetHit(
				_transform,
				_rigidbody,
				_groundDetector);

		private Landing CreateLandingMode() =>
			new Landing(
				_rigidbody,
				_groundDetector,
				_switchAnimationMode,
				_locomotionModeSwitcher,
				_playerAnimationHandler);

		private Walking CreateWalkingMode() =>
			new Walking(
				_playerLocomotionSettings.WalkingSettings,
				_camera,
				_transform,
				_rigidbody,
				_inputService,
				_groundDetector,
				_rotationHandler,
				_switchAnimationMode,
				_locomotionModeSwitcher);
	}
}