﻿using Infrastructure;
using Mechanic.Health;
using Mechanic.Player.AnimationControl;
using Service.Input;
using UnityEngine;

namespace Mechanic.Player.Locomotion
{
	[RequireComponent(typeof(PlayerLocomotion))]
	public class PlayerLocomotionDependencyCollector : MonoBehaviour,
		IDependencyCollector, IConstructor<IInputService>,
		IConstructor<PlayerAnimationHandler>, IConstructor<IHealth>
	{
		private IHealth _health;
		private IInputService _inputService;
		private PlayerLocomotion _playerLocomotion;
		private PlayerAnimationHandler _playerAnimationHandler;

		public void Awake() =>
			_playerLocomotion = GetComponent<PlayerLocomotion>();

		public void Construct(IHealth health) => 
			_health = health;

		public void Construct(IInputService inputService) => 
			_inputService = inputService;

		public void Construct(PlayerAnimationHandler playerAnimationHandler) => 
			_playerAnimationHandler = playerAnimationHandler;

		public void TransferDependencies()
		{
			_playerLocomotion
				.Construct(_health, _inputService, _playerAnimationHandler);
			
			Destroy(this);
		}
	}
}