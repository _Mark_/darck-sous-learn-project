﻿using System;
using Extension;
using UnityEngine;
using Transform = UnityEngine.Transform;

namespace Mechanic.Player.Locomotion.Handler
{
  [Serializable]
  public class Fall
  {
    [SerializeField] private float _maxSpeed;
    [SerializeField] private float _maxSpeedTime;
    [SerializeField] private AnimationCurve _fallSpeed;
    
    private float _airTime;
    private Transform _transform;

    public void Construct(Dependencies dependencies) => 
      _transform = dependencies.Transform;

    public void ResetAirTime() => 
      _airTime = 0;

    public void Update(float deltaTime) => 
      _airTime += deltaTime;

    public Vector3 VerticalVelocity()
    {
      float time = Mathf.Clamp01(_airTime / _maxSpeedTime);
      var speed = _fallSpeed.Evaluate(time) * _maxSpeed;
      return _transform.Down() * speed;
    }
  }
}