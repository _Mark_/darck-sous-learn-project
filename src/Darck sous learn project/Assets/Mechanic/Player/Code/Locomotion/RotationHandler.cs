﻿using System;
using Service.Input;
using UnityEngine;

namespace Mechanic.Player.Locomotion
{
  [Serializable]
  public class RotationHandler
  {
    [SerializeField] private float _rotationSpeed;
    
    private Transform _camera;
    private Transform _transform;
    private IInputService _inputService;

    public bool Enabled { get; set; }

    public void Construct(Transform transform, Transform camera, IInputService inputService, bool isEnabled)
    {
      Enabled = isEnabled;
      _camera = camera;
      _transform = transform;
      _inputService = inputService;
    }

    public void Handle(float delta)
    {
      if (Enabled == false)
        return;

      var direction = Direction();
      RotateTowards(direction, delta);
    }

    public void RotateTowards(Vector3 direction, float delta)
    {
      var rotation = Quaternion.LookRotation(direction);
      _transform.rotation = Quaternion.Slerp(_transform.rotation, rotation, _rotationSpeed * delta);
    }

    private Vector3 Direction()
    {
      var direction = _camera.forward * _inputService.Movement.y;
      direction += _camera.right * _inputService.Movement.x;

      if (direction == Vector3.zero)
        direction = _transform.forward;

      direction = Vector3.ProjectOnPlane(direction, _transform.up);

      return direction;
    }
  }
}