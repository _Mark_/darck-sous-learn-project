﻿namespace Mechanic.Player.Locomotion
{
	public interface ITransitionUpdate
	{
		void Update(float deltaTime);
	}
}