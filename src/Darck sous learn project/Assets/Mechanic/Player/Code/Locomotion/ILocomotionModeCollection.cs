﻿using Mechanic.Player.Locomotion.Mode;

namespace Mechanic.Player.Locomotion
{
	public interface ILocomotionModeCollection
	{
		public Walking Walking { get; }
	}
}