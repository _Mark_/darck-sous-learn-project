﻿namespace Mechanic.Player.Locomotion
{
	public interface ITransition
	{
		void MakeTransition();
		bool IsCanMakeTransition();
	}
}