﻿namespace Mechanic.Player.Locomotion
{
	public class TransitionManager
	{
		private TransitionsContainer _transitionsContainer;

		public void SwitchTransitions(TransitionsContainer transitionsContainer)
		{
			_transitionsContainer = transitionsContainer;
		}
		
		public void Update(float deltaTime)
		{
			UpdateTransitions(deltaTime);
			HandleTransition();
		}

		private void UpdateTransitions(float deltaTime)
		{
			var transitionUpdates = TransitionUpdates();
			foreach (var transitionUpdate in transitionUpdates)
				transitionUpdate.Update(deltaTime);
		}

		private void HandleTransition()
		{
			var transitions = Transitions();
			foreach (var transition in transitions)
			{
				if (transition.IsCanMakeTransition())
				{
					transition.MakeTransition();
					break;
				}
			}
		}

		private ITransition[] Transitions() => 
			_transitionsContainer.Transitions;

		private ITransitionUpdate[] TransitionUpdates() => 
			_transitionsContainer.TransitionUpdates;
	}
}