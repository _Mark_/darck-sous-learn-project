﻿namespace Mechanic.Player.Locomotion.Mode
{
	public interface IFunctional
	{
		void Update(float deltaTime);
	}
}