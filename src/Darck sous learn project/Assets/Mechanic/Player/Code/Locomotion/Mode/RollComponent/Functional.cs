﻿using Mechanic.Player.AnimationControl;
using Service.Input;
using UnityEngine;
using FallHandler = Mechanic.Player.Locomotion.Handler.Fall;

namespace Mechanic.Player.Locomotion.Mode.RollComponent
{
	public class Functional
	{
		private bool _inAir;
		private readonly Settings _settings;
		private readonly Transform _camera;
		private readonly Transform _transform;
		private readonly Rigidbody _rigidbody;
		private readonly RootMotion _rootMotion;
		private readonly FallHandler _fallHandler;
		private readonly IInputService _inputService;
		private readonly IGroundDetector _groundDetector;
		private readonly RotationHandler _rotationHandler;

		public Vector3 RollDirection { get; private set; }

		public Functional(
			Settings settings,
			Transform camera,
			Transform transform,
			Rigidbody rigidbody,
			FallHandler fallHandler,
			IInputService inputService,
			IGroundDetector groundDetector,
			RotationHandler rotationHandler,
			RootMotion rootMotion)
		{
			_settings = settings;
			_camera = camera;
			_transform = transform;
			_rigidbody = rigidbody;
			_rootMotion = rootMotion;
			_fallHandler = fallHandler;
			_inputService = inputService;
			_groundDetector = groundDetector;
			_rotationHandler = rotationHandler;
		}

		public void Start()
		{
			_inAir = false;
			_rotationHandler.Enabled = false;
			RollDirection = Direction();
		}

		public void Stop()
		{
			_rotationHandler.Enabled = true;
		}

		public void Update(float deltaTime)
		{
			_rotationHandler.RotateTowards(RollDirection, deltaTime);

			ApplyVelocity(deltaTime);
			HandleVerticalVelocity(deltaTime);
		}

		private void ApplyVelocity(float deltaTime)
		{
			var deltaPosition = _rootMotion.DeltaPosition;
			_rootMotion.ResetDeltaPosition();
			
			_rigidbody.velocity =
				deltaPosition / deltaTime * _settings.RollDistanceMultiplier;
		}

		private void HandleVerticalVelocity(float deltaTime)
		{
			var isGrounded = IsGrounded();

			if (isGrounded && _inAir)
				_inAir = false;

			if (isGrounded == false && _inAir == false)
			{
				_inAir = true;
				_fallHandler.ResetAirTime();
			}

			if (_inAir)
			{
				_fallHandler.Update(deltaTime);
				_rigidbody.velocity += _fallHandler.VerticalVelocity();
			}
		}

		private Vector3 Direction()
		{
			var direction = _camera.forward * _inputService.Movement.y;
			direction += _camera.right * _inputService.Movement.x;
			direction = Vector3.ProjectOnPlane(direction, _transform.up);
			return direction;
		}

		private bool IsGrounded() =>
			_groundDetector.IsGrounded;
	}
}