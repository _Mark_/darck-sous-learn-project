﻿using Service.Input;
using UnityEngine;
using Transform = UnityEngine.Transform;
using FallHandler = Mechanic.Player.Locomotion.Handler.Fall;

namespace Mechanic.Player.Locomotion.Mode.FallComponent
{
	public class Functional : IFunctional
	{
		private float _currentHorizontalSpeed;

		private readonly Settings _settings;
		private readonly Transform _camera;
		private readonly Transform _transform;
		private readonly Rigidbody _rigidbody;
		private readonly FallHandler _fallHandler;
		private readonly IInputService _inputService;

		public Functional(
			Transform camera,
			Settings settings,
			Transform transform,
			Rigidbody rigidbody,
			FallHandler fallHandler,
			IInputService inputService)
		{
			_camera = camera;
			_settings = settings;
			_transform = transform;
			_rigidbody = rigidbody;
			_fallHandler = fallHandler;
			_inputService = inputService;
		}

		public void Update(float deltaTime)
		{
			_fallHandler.Update(deltaTime);
			_currentHorizontalSpeed = HorizontalSpeed(deltaTime);
			Vector3 horizontalVelocity = HorizontalVelocity();
			Vector3 verticalVelocity = _fallHandler.VerticalVelocity();
			_rigidbody.velocity = verticalVelocity + horizontalVelocity;
		}

		public void ResetAirTime()
		{
			_fallHandler.ResetAirTime();
		}

		private float HorizontalSpeed(float deltaTime)
		{
			float newSpeed = _inputService.IsMovementButtonsPressed
				? _settings.HorizontalSpeed : 0;
			
			return Mathf.Lerp(_currentHorizontalSpeed, newSpeed,
				_settings.HorizontalSpeedDamper * deltaTime);
		}

		private Vector3 HorizontalVelocity()
		{
			var velocity = _camera.forward * _inputService.Movement.y;
			velocity += _camera.right * _inputService.Movement.x;
			velocity = Vector3.ProjectOnPlane(velocity, _transform.up);
			velocity.Normalize();
			velocity *= _currentHorizontalSpeed;
			return velocity;
		}
	}
}