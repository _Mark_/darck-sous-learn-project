﻿using Mechanic.Player.AnimationControl;
using UnityEngine;

namespace Mechanic.Player.Locomotion.Mode.LandingComponent
{
	public class TransitionsConstructor : Mode.TransitionsConstructor
	{
		private readonly IGroundDetector _groundDetector;
		private readonly PlayerAnimationHandler _animationHandler;
		private readonly ILocomotionModeSwitcher _locomotionModeSwitcher;

		public TransitionsConstructor(
			IGroundDetector groundDetector,
			PlayerAnimationHandler animationHandler,
			ILocomotionModeSwitcher locomotionModeSwitcher)
		{
			_groundDetector = groundDetector;
			_animationHandler = animationHandler;
			_locomotionModeSwitcher = locomotionModeSwitcher;
		}

		protected override ITransition[] CreateTransitions()
		{
			var transitions = new ITransition[]
			{
				new WalkingTransition(
					_groundDetector,
					_animationHandler,
					_locomotionModeSwitcher),
			};

			return transitions;
		}
	}
}