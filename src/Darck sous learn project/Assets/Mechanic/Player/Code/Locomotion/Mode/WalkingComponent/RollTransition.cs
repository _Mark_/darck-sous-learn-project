﻿using Infrastructure;
using Service.Input;

namespace Mechanic.Player.Locomotion.Mode.WalkingComponent
{
	public class RollTransition : BaseTransition, ITransition, IOnDestroy
	{
		private readonly IInputService _inputService;
		private readonly IGroundDetector _groundDetector;

		protected override EMode LocomotionTarget => EMode.Roll;

		public RollTransition(
			IInputService inputService,
			IGroundDetector groundDetector,
			ILocomotionModeSwitcher locomotionModeSwitcher)
			: base(locomotionModeSwitcher)
		{
			_inputService = inputService;
			_groundDetector = groundDetector;
			
			_inputService.RollPressed += OnRollPressed;
		}

		public void OnDestroy()
		{
			_inputService.RollPressed -= OnRollPressed;
		}

		public bool IsCanMakeTransition() => false;

		private void OnRollPressed()
		{
			if (_inputService.IsMovementButtonsPressed 
			    && _groundDetector.IsGrounded)
			{
				MakeTransition();
			}
		}
	}
}