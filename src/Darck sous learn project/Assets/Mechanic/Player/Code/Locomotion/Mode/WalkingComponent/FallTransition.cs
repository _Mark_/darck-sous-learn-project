﻿namespace Mechanic.Player.Locomotion.Mode.WalkingComponent
{
	public class FallTransition : BaseTransition, ITransition, ITransitionUpdate
	{
		private float _airTime;

		private readonly Settings _settings;
		private readonly IGroundDetector _groundDetector;

		protected override EMode LocomotionTarget => EMode.Fall;

		public FallTransition(
			Settings settings,
			IGroundDetector groundDetector,
			ILocomotionModeSwitcher locomotionModeSwitcher)
			: base(locomotionModeSwitcher)
		{
			_settings = settings;
			_groundDetector = groundDetector;
		}

		public void Update(float deltaTime)
		{
			bool isGrounded = _groundDetector.IsGrounded;
			if (isGrounded == false)
				_airTime += deltaTime;
			else
				_airTime = 0;
		}

		public bool IsCanMakeTransition()
		{
			bool isFlightByTime = _airTime >= _settings.FallStartTime;
			bool isPlayerCanStay = _groundDetector.IsHasStep;
			return isFlightByTime
			       || isPlayerCanStay == false;
		}
	}
}