﻿using System.Collections.Generic;

namespace Mechanic.Player.Locomotion.Mode
{
	public abstract class TransitionsConstructor
	{
		public TransitionsContainer CreateTransitionsContainer()
		{
			var transitions = CreateTransitions();
			var transitionUpdates = CreateTransitionUpdates(transitions);

			var transitionsContainer =
				new TransitionsContainer(transitions, transitionUpdates);

			return transitionsContainer;
		}

		protected abstract ITransition[] CreateTransitions();

		private ITransitionUpdate[] CreateTransitionUpdates(ITransition[] transitions)
		{
			var transitionUpdatesList = new List<ITransitionUpdate>();
			
			foreach (var transition in transitions)
			{
				if (transition is ITransitionUpdate transitionUpdate) 
					transitionUpdatesList.Add(transitionUpdate);
			}

			var transitionUpdates = transitionUpdatesList.ToArray();
			return transitionUpdates;
		}
	}
}