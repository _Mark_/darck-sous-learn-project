﻿namespace Mechanic.Player.Locomotion.Mode
{
	public interface ILocomotionUpdate
	{
		void Update(float deltaTime);
	}
}