﻿using Mechanic.Player.AnimationControl;
using Service.Input;
using UnityEngine;
using Transform = UnityEngine.Transform;
using FallHandler = Mechanic.Player.Locomotion.Handler.Fall;
using Functional = Mechanic.Player.Locomotion.Mode.FallComponent.Functional;
using Settings = Mechanic.Player.Locomotion.Mode.FallComponent.Settings;

namespace Mechanic.Player.Locomotion.Mode
{
	public class Fall : ILocomotion, ILocomotionUpdate
	{
		private readonly Functional _functional;
		private readonly ISwitchAnimationMode _switchAnimationMode;

		public TransitionsContainer Transitions { get; }

		public Fall(
			Settings settings,
			Transform camera,
			Transform transform,
			Rigidbody rigidbody,
			FallHandler fallHandler,
			IInputService inputService,
			IGroundDetector groundDetector,
			ISwitchAnimationMode switchAnimationMode,
			ILocomotionModeSwitcher locomotionModeSwitcher)
		{
			_switchAnimationMode = switchAnimationMode;

			_functional = new Functional(
				camera,
				settings,
				transform,
				rigidbody,
				fallHandler,
				inputService);

			Transitions =
				CreateTransitionsContainer(groundDetector, locomotionModeSwitcher);
		}

		public void Start()
		{
			_functional.ResetAirTime();
			_switchAnimationMode.SwitchAnimation(AnimationControl.EMode.Fall);
		}

		public void Stop() =>
			_switchAnimationMode.AnimationExit();

		public void Update(float deltaTime) =>
			_functional.Update(deltaTime);

		private TransitionsContainer CreateTransitionsContainer(
			IGroundDetector groundDetector,
			ILocomotionModeSwitcher locomotionModeSwitcher)
		{
			var transitionsConstructor = new FallComponent
				.TransitionsConstructor(groundDetector, locomotionModeSwitcher);
			var container = transitionsConstructor.CreateTransitionsContainer();
			return container;
		}
	}
}