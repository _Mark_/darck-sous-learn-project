﻿namespace Mechanic.Player.Locomotion.Mode.FallComponent
{
	public class LandingTransition : BaseTransition, ITransition
	{
		private readonly IGroundDetector _groundDetector;
		
		protected override EMode LocomotionTarget => EMode.Landing;

		public LandingTransition(
			IGroundDetector groundDetector,
			ILocomotionModeSwitcher locomotionModeSwitcher) : base(locomotionModeSwitcher)
		{
			_groundDetector = groundDetector;
		}

		public bool IsCanMakeTransition()
		{
			var isGrounded = _groundDetector.IsGrounded;
			return isGrounded;
		}

	}
}