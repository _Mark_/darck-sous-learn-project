﻿namespace Mechanic.Player.Locomotion.Mode.FallComponent
{
	public class TransitionsConstructor : Mode.TransitionsConstructor
	{
		private readonly IGroundDetector _groundDetector;
		private readonly ILocomotionModeSwitcher _locomotionModeSwitcher;

		public TransitionsConstructor(
			IGroundDetector groundDetector,
			ILocomotionModeSwitcher locomotionModeSwitcher)
		{
			_groundDetector = groundDetector;
			_locomotionModeSwitcher = locomotionModeSwitcher;
		}

		protected override ITransition[] CreateTransitions()
		{
			var transitions = new ITransition[]
			{
				new LandingTransition(_groundDetector, _locomotionModeSwitcher),
			};
			
			return transitions;
		}
	}
}