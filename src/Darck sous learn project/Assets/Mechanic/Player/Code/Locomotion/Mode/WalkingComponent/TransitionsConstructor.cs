﻿using System.Collections.Generic;
using Infrastructure;
using Service.Input;

namespace Mechanic.Player.Locomotion.Mode.WalkingComponent
{
	public class TransitionsConstructor : Mode.TransitionsConstructor
	{
		private readonly Settings _settings;
		private readonly IInputService _inputService;
		private readonly IGroundDetector _groundDetector;
		private readonly List<IOnDestroy> _destroyList;
		private readonly ILocomotionModeSwitcher _locomotionModeSwitcher;

		public TransitionsConstructor(
			Settings settings,
			IInputService inputService,
			IGroundDetector groundDetector,
			List<IOnDestroy> destroyList,
			ILocomotionModeSwitcher locomotionModeSwitcher)
		{
			_settings = settings;
			_destroyList = destroyList;
			_inputService = inputService;
			_groundDetector = groundDetector;
			_locomotionModeSwitcher = locomotionModeSwitcher;
		}

		protected override ITransition[] CreateTransitions()
		{
			var rollTransition = CreateRollTransition();
			var fallTransition = CreateFallTransition();

			var transitions = new ITransition[]
			{
				fallTransition,
				rollTransition,
			};
			return transitions;
		}

		private RollTransition CreateRollTransition()
		{
			var rollTransition = new RollTransition(
				_inputService,
				_groundDetector,
				_locomotionModeSwitcher);
			_destroyList.Add(rollTransition);

			return rollTransition;
		}

		private FallTransition CreateFallTransition() => 
			new FallTransition(_settings, _groundDetector, _locomotionModeSwitcher);
	}
}