﻿using Mechanic.Player.AnimationControl;

namespace Mechanic.Player.Locomotion.Mode.RollComponent
{
	public class WalkingTransition : BaseTransition, ITransition
	{
		protected override EMode LocomotionTarget => EMode.Walking;

		private readonly ISwitchAnimationMode _switchAnimationMode;
		private readonly PlayerAnimationHandler _playerAnimationHandler;

		public WalkingTransition(
			PlayerAnimationHandler playerAnimationHandler,
			ILocomotionModeSwitcher locomotionModeSwitcher)
			: base(locomotionModeSwitcher)
		{
			_playerAnimationHandler = playerAnimationHandler;
		}

		public bool IsCanMakeTransition()
		{
			_playerAnimationHandler.Animation[AnimationControl.EMode.Roll].IsActive();
		}
	}
}