﻿using System;

namespace Mechanic.Player.Locomotion.Mode.RollComponent
{
	[Serializable]
	public class Settings
	{
		public float RollDistanceMultiplier = 1;
	}
}