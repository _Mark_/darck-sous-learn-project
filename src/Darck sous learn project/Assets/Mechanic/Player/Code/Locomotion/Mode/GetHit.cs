﻿using System;
using UnityEngine;

namespace Mechanic.Player.Locomotion.Mode
{
	public class GetHit : ILocomotion
	{
		[SerializeField] private float _normalizeVerticalPositionDamper;

		private Transform _transform;
		private Rigidbody _rigidbody;
		private IGroundDetector _groundDetector;

		public GetHit(
			Transform transform,
			Rigidbody rigidbody,
			IGroundDetector groundDetector)
		{
			_transform = transform;
			_rigidbody = rigidbody;
			_groundDetector = groundDetector;
		}

		public TransitionsContainer Transitions { get; }

		public void Start()
		{
			_rigidbody.velocity = Vector3.zero;
		}

		public void Stop() { }

		public void Update(float deltaTime)
		{
			NormalizeVerticalPosition(deltaTime);
		}

		private void NormalizeVerticalPosition(float deltaTime)
		{
			if (IsHasStep())
			{
				var tr = _transform;
				var position = tr.position;
				var offset = _groundDetector.Hit.point - position;
				offset = Vector3.Project(offset, tr.up);

				var target = Vector3.Lerp(position, position + offset, deltaTime * _normalizeVerticalPositionDamper);
				_rigidbody.MovePosition(target);
			}
		}

		private bool IsHasStep() =>
			_groundDetector.IsHasStep;
	}
}