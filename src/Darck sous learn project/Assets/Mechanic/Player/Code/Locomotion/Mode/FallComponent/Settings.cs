﻿using System;
using UnityEngine;

namespace Mechanic.Player.Locomotion.Mode.FallComponent
{
	[Serializable]
	public class Settings
	{
		public float HorizontalSpeed;
		public float HorizontalSpeedDamper;
	}
}