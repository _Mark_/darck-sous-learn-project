﻿using System;
using UnityEngine;

namespace Mechanic.Player.Locomotion.Mode.WalkingComponent
{
	[Serializable]
	public class Settings
	{
		public float WalkSpeed;
		public float RunSpeed;
		public float SprintSpeed;
		[Space]
		public float SpeedDamper;
		public float NormalizeVerticalPositionDamper;
		[Header("Slope")]
		public float SlidingSpeed;
		public float AvailableSlope;
		[Header("Transition")]
		public float FallStartTime;
	}
}