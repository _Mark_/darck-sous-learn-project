﻿using Mechanic.Player.AnimationControl;
using UnityEngine;

namespace Mechanic.Player.Locomotion.Mode
{
  public class Landing : ILocomotion
  {
    private readonly Rigidbody _rigidbody;
    private readonly ISwitchAnimationMode _switchAnimationMode;

    public TransitionsContainer Transitions { get; }
    
    public Landing(
      Rigidbody rigidbody,
      IGroundDetector groundDetector,
      ISwitchAnimationMode switchAnimationMode,
      ILocomotionModeSwitcher locomotionModeSwitcher,
      PlayerAnimationHandler animationHandler)
    {
      _rigidbody = rigidbody;
      _switchAnimationMode = switchAnimationMode;

      Transitions = CreateTransitions(
        groundDetector,
        animationHandler,
        locomotionModeSwitcher);
    }

    private static TransitionsContainer CreateTransitions(
      IGroundDetector groundDetector,
      PlayerAnimationHandler animationHandler,
      ILocomotionModeSwitcher locomotionModeSwitcher)
    {
      var transitionsConstructor =
        new LandingComponent.TransitionsConstructor(
          groundDetector,
          animationHandler,
          locomotionModeSwitcher);
      var transitionsContainer = transitionsConstructor.CreateTransitionsContainer();
      return transitionsContainer;
    }

    public void Start()
    {
      _rigidbody.velocity = Vector3.zero;
      _switchAnimationMode.SwitchAnimation(AnimationControl.EMode.Landing);
    }

    public void Stop() { }
  }
}