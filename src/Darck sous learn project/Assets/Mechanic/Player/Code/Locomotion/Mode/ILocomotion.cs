﻿namespace Mechanic.Player.Locomotion.Mode
{
  public interface ILocomotion
  {
    TransitionsContainer Transitions { get; }
    void Start();
    void Stop();
  }
}