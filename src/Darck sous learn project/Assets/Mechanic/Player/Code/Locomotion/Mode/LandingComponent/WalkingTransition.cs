﻿using Mechanic.Player.AnimationControl;

namespace Mechanic.Player.Locomotion.Mode.LandingComponent
{
	public class WalkingTransition : BaseTransition, ITransition
	{
		protected override EMode LocomotionTarget => EMode.Walking;

		private readonly IGroundDetector _groundDetector;
		private readonly PlayerAnimationHandler _animationHandler;

		public WalkingTransition(
			IGroundDetector groundDetector,
			PlayerAnimationHandler animationHandler,
			ILocomotionModeSwitcher locomotionModeSwitcher) : base(locomotionModeSwitcher)
		{
			_groundDetector = groundDetector;
			_animationHandler = animationHandler;
		}

		public bool IsCanMakeTransition()
		{
			var isGrounded = _groundDetector.IsGrounded;
			var isLandingAnimationEnd = _animationHandler.Landing.IsActive();
			
			return isGrounded 
			       && isLandingAnimationEnd == false;
		}
	}
}