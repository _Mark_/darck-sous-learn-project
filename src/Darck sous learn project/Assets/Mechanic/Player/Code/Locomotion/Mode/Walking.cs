﻿using System.Collections.Generic;
using Infrastructure;
using Mechanic.Player.AnimationControl;
using Mechanic.Player.Locomotion.Mode.WalkingComponent;
using Service.Input;
using UnityEngine;
using Transform = UnityEngine.Transform;

namespace Mechanic.Player.Locomotion.Mode
{
	public class Walking : ILocomotion, ILocomotionUpdate, IOnDestroy
	{
		private const AnimationControl.EMode ANIMATION_MODE =
			AnimationControl.EMode.Walking;

		private readonly Settings _settings;
		private readonly Functional _functional;
		private readonly List<IOnDestroy> _destroyList = new List<IOnDestroy>();
		private readonly ISwitchAnimationMode _switchAnimationMode;

		public float MaxSpeed => _settings.SprintSpeed;
		public float CurrentSpeed => _functional.CurrentSpeed;
		public TransitionsContainer Transitions { get; private set; }

		public Walking(
			Settings settings,
			Transform camera,
			Transform transform,
			Rigidbody rigidbody,
			IInputService inputService,
			IGroundDetector groundDetector,
			RotationHandler rotationHandler,
			ISwitchAnimationMode switchAnimationMode,
			ILocomotionModeSwitcher locomotionModeSwitcher)
		{
			_settings = settings;
			_switchAnimationMode = switchAnimationMode;

			_functional = CreateFunctional(
				camera,
				transform,
				rigidbody,
				inputService,
				groundDetector,
				rotationHandler);

			CreateTransitions(inputService, groundDetector, locomotionModeSwitcher);
		}

		public void OnDestroy() =>
			_destroyList.ForEach(element => element.OnDestroy());

		public void Start() => 
			_switchAnimationMode.SwitchAnimation(ANIMATION_MODE);

		public void Stop() => 
			_switchAnimationMode.AnimationExit();

		public void Update(float deltaTime) => 
			_functional.Update(deltaTime);

		private void CreateTransitions(
			IInputService inputService,
			IGroundDetector groundDetector,
			ILocomotionModeSwitcher locomotionModeSwitcher)
		{
			var transitionsConstructor = 
				new WalkingComponent.TransitionsConstructor(
					_settings,
					inputService,
					groundDetector,
					_destroyList,
					locomotionModeSwitcher);
			
			Transitions = transitionsConstructor.CreateTransitionsContainer();
		}

		private Functional CreateFunctional(
			Transform camera,
			Transform transform,
			Rigidbody rigidbody,
			IInputService inputService,
			IGroundDetector groundDetector,
			RotationHandler rotationHandler)
		{
			var functional = new Functional(
				_settings,
				camera,
				transform,
				rigidbody,
				inputService,
				groundDetector,
				rotationHandler);
			_destroyList.Add(functional);

			return functional;
		}
	}
}