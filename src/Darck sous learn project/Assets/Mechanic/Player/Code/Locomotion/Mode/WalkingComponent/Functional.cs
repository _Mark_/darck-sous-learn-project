﻿using Infrastructure;
using Service.Input;
using UnityEngine;

namespace Mechanic.Player.Locomotion.Mode.WalkingComponent
{
	public class Functional : IFunctional, IOnDestroy
	{
		private bool _runMode;

		private readonly Settings _settings;
		private readonly Transform _camera;
		private readonly Transform _transform;
		private readonly Rigidbody _rigidbody;
		private readonly IInputService _inputService;
		private readonly IGroundDetector _groundDetector;
		private readonly RotationHandler _rotationHandler;

		public float CurrentSpeed { get; private set; }

		public Functional(
			Settings settings,
			Transform camera,
			Transform transform,
			Rigidbody rigidbody,
			IInputService inputService,
			IGroundDetector groundDetector,
			RotationHandler rotationHandler)
		{
			_settings = settings;
			_camera = camera;
			_transform = transform;
			_rigidbody = rigidbody;
			_inputService = inputService;
			_groundDetector = groundDetector;
			_rotationHandler = rotationHandler;

			_inputService.RunModePressed += OnRunModePressed;
		}

		public void OnDestroy()
		{
			_inputService.RunModePressed -= OnRunModePressed;
		}

		public void Update(float deltaTime)
		{
			_rotationHandler.Handle(deltaTime);

			CurrentSpeed = CalculateSpeed(deltaTime);
			var surfaceNormal = _groundDetector.Hit.normal;

			if (IsSlopeAvailable(surfaceNormal))
				Movement(deltaTime, surfaceNormal);
			else
				SlidingAlongSlope(surfaceNormal);
		}

		private void Movement(float deltaTime, Vector3 surfaceNormal)
		{
			ApplyVelocity(surfaceNormal);
			NormalizeVerticalPosition(deltaTime);
		}

		private void SlidingAlongSlope(Vector3 surfaceNormal)
		{
			var slidingDirection =
				Vector3.ProjectOnPlane(surfaceNormal, _transform.up);
			slidingDirection =
				Vector3.ProjectOnPlane(slidingDirection, surfaceNormal);
			_rigidbody.velocity =
				slidingDirection.normalized * _settings.SlidingSpeed;
		}

		private float CalculateSpeed(float deltaTime)
		{
			float newSpeed;
			if (_inputService.IsMovementButtonsPressed == false)
				newSpeed = 0;
			else if (_runMode)
				newSpeed = _settings.WalkSpeed;
			else
				newSpeed = _inputService.IsSprintPressed ? _settings.SprintSpeed
					: _settings.RunSpeed;

			return Mathf.Lerp(CurrentSpeed, newSpeed,
				_settings.SpeedDamper * deltaTime);
		}

		private void ApplyVelocity(Vector3 surfaceNormal)
		{
			var velocity = Velocity(CurrentSpeed, surfaceNormal);
			_rigidbody.velocity = velocity;
		}

		private void NormalizeVerticalPosition(float deltaTime)
		{
			if (IsHasStep())
			{
				var tr = _transform;
				var position = tr.position;
				var offset = _groundDetector.Hit.point - position;
				offset = Vector3.Project(offset, tr.up);

				var target = Vector3.Lerp(position, position + offset,
					deltaTime * _settings.NormalizeVerticalPositionDamper);
				_rigidbody.MovePosition(target);
			}
		}

		private Vector3 Velocity(float speed, Vector3 surfaceNormal)
		{
			var velocity = _camera.forward * _inputService.Movement.y;
			velocity += _camera.right * _inputService.Movement.x;
			velocity = Vector3.ProjectOnPlane(velocity, surfaceNormal);
			velocity.Normalize();
			velocity *= speed;
			return velocity;
		}

		private void OnRunModePressed() =>
			_runMode = !_runMode;

		private bool IsSlopeAvailable(Vector3 surfaceNormal) =>
			Vector3.Angle(surfaceNormal, _transform.up) < _settings.AvailableSlope;

		private bool IsHasStep() =>
			_groundDetector.IsHasStep;
	}
}