﻿using Mechanic.Player.AnimationControl;
using Mechanic.Player.Locomotion.Mode.RollComponent;
using Service.Input;
using UnityEngine;
using FallHandler = Mechanic.Player.Locomotion.Handler.Fall;

namespace Mechanic.Player.Locomotion.Mode
{
	public class Roll : ILocomotion, ITransitionUpdate
	{
		private const AnimationControl.EMode ANIMATION_MODE =
			AnimationControl.EMode.Roll;

		private Settings _settings;

		private readonly Functional _functional;
		private readonly ISwitchAnimationMode _switchAnimationMode;

		public TransitionsContainer Transitions { get; }

		public Roll(
			Transform camera,
			Transform transform,
			Rigidbody rigidbody,
			FallHandler fallHandler,
			IInputService inputService,
			IGroundDetector groundDetector,
			RotationHandler rotationHandler,
			IRootMotionContainer rootMotionContainer,
			ISwitchAnimationMode switchAnimationMode,
			PlayerAnimationHandler animationHandler,
			ILocomotionModeSwitcher locomotionModeSwitcher)
		{
			_switchAnimationMode = switchAnimationMode;

			_functional = new Functional(
				_settings,
				camera,
				transform,
				rigidbody,
				fallHandler,
				inputService,
				groundDetector,
				rotationHandler,
				rootMotionContainer);

			var transitionsConstructor = new RollComponent
				.TransitionsConstructor(
					groundDetector,
					animationHandler,
					locomotionModeSwitcher);
			Transitions = transitionsConstructor.CreateTransitionsContainer();
		}

		public void Start()
		{
			_functional.Start();
			_switchAnimationMode.SwitchAnimation(ANIMATION_MODE);
		}

		public void Stop()
		{
			_functional.Stop();
		}

		public void Update(float deltaTime) =>
			_functional.Update(deltaTime);
	}
}