﻿namespace Mechanic.Player.Locomotion
{
	public interface ILocomotionModeSwitcher
	{
		void SwitchToMode(EMode mode);
	}
}