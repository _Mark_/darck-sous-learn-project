﻿namespace Mechanic.Player.Locomotion
{
  public enum EMode
  {
    None,
    Fall,
    Roll,
    Death,
    Attack,
    GetHit,
    Walking,
    Landing,
  }
}