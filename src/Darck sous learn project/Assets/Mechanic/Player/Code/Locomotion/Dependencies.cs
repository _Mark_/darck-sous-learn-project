﻿using System.Collections.Generic;
using Infrastructure;
using Mechanic.Player.AnimationControl;
using Service.Input;
using UnityEngine;
using FallHandler = Mechanic.Player.Locomotion.Handler.Fall;

namespace Mechanic.Player.Locomotion
{
	public class Dependencies
	{
		public readonly Transform Camera;
		public readonly Transform Transform;
		public readonly Rigidbody Rigidbody;
		public readonly FallHandler FallHandler;
		public readonly IInputService InputService;
		public readonly IGroundDetector GroundDetector;
		public readonly RotationHandler RotationHandler;
		public readonly List<IOnDestroy> DestructionList;
		public readonly PlayerAnimationHandler PlayerAnimationHandler;
		public readonly ILocomotionModeSwitcher LocomotionModeSwitcher;
		public readonly PlayerLocomotionSettings PlayerLocomotionSettings;

		public Dependencies(
			Transform camera,
			Transform transform,
			Rigidbody rigidbody,
			FallHandler fallHandler,
			IInputService inputService,
			IGroundDetector groundDetector,
			RotationHandler rotationHandler,
			CapsuleCollider capsuleCollider,
			List<IOnDestroy> destructionList,
			PlayerAnimationHandler playerAnimationHandler,
			ILocomotionModeSwitcher locomotionModeSwitcher,
			PlayerLocomotionSettings playerLocomotionSettings)
		{
			Camera = camera;
			Transform = transform;
			Rigidbody = rigidbody;
			FallHandler = fallHandler;
			InputService = inputService;
			GroundDetector = groundDetector;
			RotationHandler = rotationHandler;
			DestructionList = destructionList;
			PlayerAnimationHandler = playerAnimationHandler;
			LocomotionModeSwitcher = locomotionModeSwitcher;
			PlayerLocomotionSettings = playerLocomotionSettings;
		}
	}
}