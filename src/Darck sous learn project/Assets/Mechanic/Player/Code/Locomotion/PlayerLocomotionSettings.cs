﻿using System;
using UnityEngine;
using FallSettings = Mechanic.Player.Locomotion.Mode.FallComponent.Settings;
using WalkingSettings = Mechanic.Player.Locomotion.Mode.WalkingComponent.Settings;

namespace Mechanic.Player.Locomotion
{
	[Serializable]
	public class PlayerLocomotionSettings
	{
		[SerializeField] private FallSettings _fallSettings;
		[SerializeField] private WalkingSettings _walkingSettings;
		
		public FallSettings FallSettings => _fallSettings;
		public WalkingSettings WalkingSettings => _walkingSettings;
	}
}