﻿using Infrastructure;
using Mechanic.Player.AnimationControl;
using Mechanic.Player.Locomotion;
using Mechanic.WeaponSlot;
using Service.Input;
using UnityEngine;
using EMode = Mechanic.Player.Locomotion.EMode;

namespace Mechanic.Player.Combat
{
	public class PlayerAttack : MonoBehaviour,
		IConstructor, IConstructor<IInputService>, IConstructor<PlayerInventory>,
		IConstructor<PlayerLocomotion>, IConstructor<PlayerAnimationHandler>
	{
		private IInputService _inputService;
		private PlayerInventory _playerInventory;
		private PlayerLocomotion _playerLocomotion;
		private PlayerAnimationHandler _playerAnimationHandler;

		private void OnDestroy()
		{
			_inputService.LmbPressed += OnLmbPressed;
			_inputService.RmbPressed += OnRmbPressed;
		}

		public void Construct(IInputService inputService) =>
			_inputService = inputService;

		public void Construct(PlayerInventory playerInventory) =>
			_playerInventory = playerInventory;

		public void Construct(PlayerLocomotion playerLocomotion) =>
			_playerLocomotion = playerLocomotion;

		public void Construct(PlayerAnimationHandler playerAnimationHandler) =>
			_playerAnimationHandler = playerAnimationHandler;

		public void Construct()
		{
			_inputService.LmbPressed += OnLmbPressed;
			_inputService.RmbPressed += OnRmbPressed;
		}

		public void FinishAttack() =>
			_playerLocomotion.ModeController.SwitchToNoneMode();

		private void OnLmbPressed()
		{
			if (IsHasWeapon(SlotSide.Right) == false ||
			    IsCanLaunchAttack() == false)
				return;

			_playerLocomotion.ModeController.SwitchToMode(EMode.Attack);
			_playerAnimationHandler.Attack.LaunchSwordAttackRightHand();
		}

		private void OnRmbPressed()
		{
			if (IsHasWeapon(SlotSide.Left) == false ||
			    IsCanLaunchAttack() == false)
				return;

			_playerLocomotion.ModeController.SwitchToMode(EMode.Attack);
			_playerAnimationHandler.Attack.LaunchSwordAttackLeftHand();
		}

		private bool IsHasWeapon(SlotSide side) =>
			_playerInventory.HandsSlots.Slot(side).IsHasWeapon;

		private bool IsCanLaunchAttack() =>
			_playerLocomotion.ModeController.Mode != EMode.Fall &&
			_playerLocomotion.ModeController.Mode != EMode.Landing &&
			_playerLocomotion.ModeController.Mode != EMode.Roll;
	}
}