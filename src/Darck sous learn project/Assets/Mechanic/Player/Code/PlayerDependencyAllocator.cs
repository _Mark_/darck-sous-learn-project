﻿using Infrastructure;
using Mechanic.Health;
using Mechanic.Player.AnimationControl;
using Mechanic.Player.Combat;
using Mechanic.Player.Locomotion;
using UnityEngine;

namespace Mechanic.Player
{
  public class PlayerDependencyAllocator : MonoBehaviour,
    ILocalDependencyAllocator
  {
    private Health.Health _health;
    
    public void InjectDependency()
    {
      Allocate<IHealth>();
      Allocate<PlayerAttack>();
      Allocate<PlayerLocomotion>();
      Allocate<PlayerAnimationHandler>();
      
      Destroy(this);
    }

    private void Allocate<T>()
    {
      var component = GetComponentInChildren<T>();
      foreach (var constructor in GetComponentsInChildren<IConstructor<T>>())
        constructor.Construct(component);
    }
  }
}