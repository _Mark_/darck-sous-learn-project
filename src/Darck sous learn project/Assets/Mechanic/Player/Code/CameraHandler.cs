﻿using Infrastructure;
using Service.Input;
using UnityEngine;

namespace Mechanic.Player
{
	public class CameraHandler : MonoBehaviour, IConstructor<IInputService>
	{
		private const int COLLISION_HIT_QUANTITY = 1;
		private const float SMOOTH_DAMP_MAX_SPEED = float.PositiveInfinity;

		[SerializeField] private Transform _camera;
		[SerializeField] private Transform _target;
		[SerializeField] private Transform _cameraPivot;
		[Space]
		[SerializeField] private float _collisionSphereRadius = .5f;
		[SerializeField] private LayerMask _ignoreLayers;
		[SerializeField] private float _handleCollisionTime = 1;
		[Space]
		[SerializeField] private float _followTime;
		[SerializeField] private float _verticalLookSpeed;
		[SerializeField] private float _horizontalLookSpeed;
		[Space]
		[SerializeField] private float verticalMin = -90;
		[SerializeField] private float verticalMax = 90;
		[Space]
		[SerializeField] private float _rotationDamper;

		private float _verticalLookAngle;
		private float _horizontalLookAngle;
		private float _defaultCameraOffsetZ;
		private float _handleCollisionVelocity;
		private Vector3 _followCameraVelocity;
		private RaycastHit[] _collisionHits;
		private IInputService _inputService;

		private void Awake()
		{
			_collisionHits = new RaycastHit[COLLISION_HIT_QUANTITY];
			_defaultCameraOffsetZ = _camera.localPosition.z;
		}

		private void LateUpdate()
		{
			var delta = Time.deltaTime;
			FollowToTarget(delta);
			HandleCollision(delta);
			Rotate(delta);
		}

		public void Construct(IInputService inputService) =>
			_inputService = inputService;

		private void FollowToTarget(float delta) =>
			transform.position = Vector3.SmoothDamp(transform.position, _target.position, ref _followCameraVelocity, _followTime,
				SMOOTH_DAMP_MAX_SPEED, delta);

		private void Rotate(float delta)
		{
			_verticalLookAngle -= _inputService.Camera.y * _verticalLookSpeed * delta;
			_horizontalLookAngle += _inputService.Camera.x * _horizontalLookSpeed * delta;

			_verticalLookAngle = Mathf.Clamp(_verticalLookAngle, verticalMin, verticalMax);

			var direction = new Vector3(0, _horizontalLookAngle, 0);
			var rotation = Quaternion.Euler(direction);
			transform.rotation = Quaternion.Slerp(transform.rotation, rotation, _rotationDamper * delta);

			direction = new Vector3(_verticalLookAngle, 0, 0);
			rotation = Quaternion.Euler(direction);
			_cameraPivot.localRotation = Quaternion.Slerp(_cameraPivot.localRotation, rotation, _rotationDamper * delta);
		}

		private void HandleCollision(float delta)
		{
			float targetOffsetZ = _defaultCameraOffsetZ;
			if (SphereCastHit())
			{
				var hit = _collisionHits[0];
				var distance = Vector3.Distance(_cameraPivot.position, hit.point);
				targetOffsetZ = _collisionSphereRadius - distance;
			}

			var cameraLocalPosition = _camera.localPosition;
			targetOffsetZ =
				Mathf.SmoothDamp(cameraLocalPosition.z, targetOffsetZ, ref _handleCollisionVelocity,
					_handleCollisionTime, SMOOTH_DAMP_MAX_SPEED, delta);
			_camera.localPosition = new Vector3(cameraLocalPosition.x, cameraLocalPosition.y, targetOffsetZ);
		}

		private bool SphereCastHit()
		{
			var origin = _cameraPivot.position;
			var castDirection = _camera.position - origin;
			castDirection.Normalize();
			var collisionLayers = ~_ignoreLayers;

			var hitQuantity =
				Physics.SphereCastNonAlloc(origin, _collisionSphereRadius, castDirection,
					_collisionHits, -_defaultCameraOffsetZ, collisionLayers, QueryTriggerInteraction.Ignore);
			if (hitQuantity == 0)
				return false;

			return true;
		}
	}
}