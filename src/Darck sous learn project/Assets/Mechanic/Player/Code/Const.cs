﻿namespace Mechanic.Player
{
  public static class Const
  {
    public static class Animator
    {
      public static class Parameter
      {
        public const string Z_VELOCITY = "z velocity";
        public const string X_VELOCITY = "x velocity";
      }
    }

    public static class Animation
    {
      public const string GET_HIT = "Get hit";
      public const string LANDING = "Landing";
      public const string FALLING = "Falling";
      public const string WALKING = "Walking";
      public const string ROLL_FORWARD = "Roll forward";
      public const string SWORD_ATTACK_LEFT_HAND = "Sword attack left hand";
      public const string SWORD_ATTACK_RIGHT_HAND = "Sword attack right hand";

      public static class Layer
      {
        public const string ATTACK = "Attack";
      }
    }
  }
}