﻿using Mechanic.Player.Locomotion;

namespace Mechanic.Player.AnimationControl
{
	public interface ISwitchAnimationMode
	{
		void AnimationExit();
		void SwitchAnimation(EMode mode);
	}
}