﻿namespace Mechanic.Player.AnimationControl
{
	public enum EMode
	{
		None,
		Fall,
		Roll,
		Death,
		Attack,
		GetHit,
		Walking,
		Landing,
	}
}