﻿using Infrastructure;
using Mechanic.Player.Combat;
using Mechanic.Player.Locomotion;
using UnityEngine;

namespace Mechanic.Player.AnimationControl
{
	[RequireComponent(typeof(Animator))]
	public sealed class PlayerAnimationHandler : MonoBehaviour, IConstructor,
		IConstructor<PlayerLocomotion>, ISwitchAnimationMode,
		IAnimationStateExitReader
	{
		private Animator _animator;
		private ModeSwitcher _modeSwitcher;
		private PlayerAttack _playerAttack;
		private PlayerLocomotion _playerLocomotion;
		private AnimationCollection _animationCollection;

		public RootMotion RootMotion { get; private set; }

		private void Awake()
		{
			_animator = GetComponent<Animator>();
			RootMotion = new RootMotion(_animator);
		}

		private void Update()
		{
			if (_modeSwitcher.IsCanUpdateAnimation)
				_modeSwitcher.ActiveUpdateAnimation.Update();
		}

		private void OnAnimatorMove()
		{
			if (RootMotion.Enabled)
				RootMotion.AddToDeltaPosition(_animator.deltaPosition);
		}

		public void Construct(PlayerLocomotion playerLocomotion) =>
			_playerLocomotion = playerLocomotion;

		public void Construct()
		{
			CreateAnimationCollection();
			CreateModeSwitcher();
		}

		public void AnimationExit() =>
			_modeSwitcher.DeactivateActiveAnimation();

		public void SwitchAnimation(EMode mode) =>
			_modeSwitcher.ActivateMode(mode);

		private void CreateAnimationCollection()
		{
			_animationCollection = new AnimationCollection(
				_animator,
				RootMotion,
				_playerLocomotion);
		}

		private void CreateModeSwitcher() =>
			_modeSwitcher = new ModeSwitcher(_animationCollection);
	}
}