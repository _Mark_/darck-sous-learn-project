﻿using System.Collections.Generic;
using Mechanic.Player.AnimationControl.Mode;
using Mechanic.Player.Locomotion;
using UnityEngine;
using Animation = Mechanic.Player.AnimationControl.Mode.Animation;

namespace Mechanic.Player.AnimationControl
{
	public sealed class AnimationCollection
	{
		private readonly Dictionary<EMode, Animation> _animationModes =
			new Dictionary<EMode, Animation>();
		private readonly Dictionary<EMode, IAnimationUpdate> _updatedAnimations =
			new Dictionary<EMode, IAnimationUpdate>();
		private readonly Dictionary<EMode, IAnimationLifeCycle> _lifeCycleModes =
			new Dictionary<EMode, IAnimationLifeCycle>();

		public AnimationCollection(
			Animator animator,
			RootMotion rootMotion,
			PlayerLocomotion playerLocomotion)
		{
			RegisterAnimations(animator, rootMotion, playerLocomotion);
		}

		public Roll Roll { get; private set; }
		public Attack Attack { get; private set; }
		public Landing Landing { get; private set; }

		public IReadOnlyDictionary<EMode, Animation> AnimationModes =>
			_animationModes;
		public IReadOnlyDictionary<EMode, IAnimationUpdate> UpdatedAnimations =>
			_updatedAnimations;
		public IReadOnlyDictionary<EMode, IAnimationLifeCycle> LifeCycleModes =>
			_lifeCycleModes;

		private void RegisterAnimations(
			Animator animator,
			RootMotion rootMotion,
			PlayerLocomotion playerLocomotion)
		{
			var animationRegistrar = new AnimationRegistrar(
				animator,
				rootMotion,
				playerLocomotion,
				_animationModes,
				_updatedAnimations,
				_lifeCycleModes);

			animationRegistrar.Register();
			Roll = animationRegistrar.Roll;
			Attack = animationRegistrar.Attack;
			Landing = animationRegistrar.Landing;
		}
	}
}