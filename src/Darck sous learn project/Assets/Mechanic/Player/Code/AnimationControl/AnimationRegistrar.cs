﻿using System.Collections.Generic;
using Mechanic.Player.AnimationControl.Mode;
using Mechanic.Player.Locomotion;
using UnityEngine;
using Extension;
using Animation = Mechanic.Player.AnimationControl.Mode.Animation;

namespace Mechanic.Player.AnimationControl
{
	public class AnimationRegistrar
	{
		private Fall _fall;
		private Roll _roll;
		private GetHit _getHit;
		private Attack _attack;
		private Walking _walking;
		private Landing _landing;

		private readonly Animator _animator;
		private readonly RootMotion _rootMotion;
		private readonly PlayerLocomotion _playerLocomotion;
		private readonly Dictionary<EMode, Animation> _animationModes;
		private readonly Dictionary<EMode, IAnimationUpdate> _updatedAnimations;
		private readonly Dictionary<EMode, IAnimationLifeCycle> _lifeCycleModes;

		public AnimationRegistrar(
			Animator animator,
			RootMotion rootMotion,
			PlayerLocomotion playerLocomotion,
			Dictionary<EMode, Animation> animationModes,
			Dictionary<EMode, IAnimationUpdate> updatedAnimations,
			Dictionary<EMode, IAnimationLifeCycle> lifeCycleModes)
		{
			_animator = animator;
			_rootMotion = rootMotion;
			_lifeCycleModes = lifeCycleModes;
			_animationModes = animationModes;
			_playerLocomotion = playerLocomotion;
			_updatedAnimations = updatedAnimations;
		}

		public Roll Roll => _roll;
		public Attack Attack => _attack;
		public Landing Landing => _landing;

		public void Register()
		{
			CreateAnimations();
			FillAnimationsModDictionary();
			FillLifeCycleModes();
			FillUpdatedAnimationsDictionary();
		}

		private void CreateAnimations()
		{
			_fall = new Fall(_animator);
			_roll = new Roll(_animator, _rootMotion);
			_getHit = new GetHit(_animator, _playerLocomotion);
			_attack = new Attack(_animator);
			_landing = new Landing(_animator);
			_walking = new Walking(_animator, _playerLocomotion);
		}

		private void FillAnimationsModDictionary()
		{
			_animationModes
				.AddNew(EMode.Fall, _fall)
				.AddNew(EMode.Roll, _roll)
				.AddNew(EMode.GetHit, _getHit)
				.AddNew(EMode.Attack, _attack)
				.AddNew(EMode.Landing, _landing)
				.AddNew(EMode.Walking, _walking);
		}

		private void FillLifeCycleModes()
		{
			foreach (var animationMode in _animationModes)
			{
				var mode = animationMode.Key;
				var animation = animationMode.Value;
				_lifeCycleModes.Add(mode, animation);
			}
		}

		private void FillUpdatedAnimationsDictionary()
		{
			foreach (var pair in _animationModes)
			{
				var mode = pair.Key;
				var animation = pair.Value;
				if (animation is IAnimationUpdate update)
					_updatedAnimations.Add(mode, update);
			}
		}
	}
}