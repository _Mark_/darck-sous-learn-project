﻿using System.Collections.Generic;
using Mechanic.Player.AnimationControl.Mode;

namespace Mechanic.Player.AnimationControl
{
	public interface IAnimationCollection
	{
		IReadOnlyDictionary<EMode, Animation> AnimationModes { get; }
		IReadOnlyDictionary<EMode, IAnimationUpdate> UpdatedAnimations { get; }
		IReadOnlyDictionary<EMode, IAnimationLifeCycle> LifeCycleModes { get; }
	}
}