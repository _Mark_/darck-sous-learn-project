﻿namespace Mechanic.Player.AnimationControl.Mode
{
	public interface IAnimationLifeCycle
	{
		void Activate();
		void Deactivate();
	}
}