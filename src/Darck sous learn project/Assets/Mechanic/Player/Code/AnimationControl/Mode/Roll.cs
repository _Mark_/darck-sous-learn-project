﻿using UnityEngine;

namespace Mechanic.Player.AnimationControl.Mode
{
	public class Roll : Animation
	{
		private readonly RootMotion _rootMotion;

		public Roll(Animator animator, RootMotion rootMotion) 
			: base(animator, Animator.StringToHash(Const.Animation.ROLL_FORWARD))
		{
			_rootMotion = rootMotion;
		}

		protected override void OnActivating() => 
			_rootMotion.Enabled = true;

		protected override void OnDeactivating() => 
			_rootMotion.Enabled = false;
	}
}