﻿using Mechanic.Player.Locomotion;
using UnityEngine;

namespace Mechanic.Player.AnimationControl.Mode
{
	public class Walking : Animation, IAnimationUpdate
	{
		private readonly int _zVelocity =
			Animator.StringToHash(Const.Animator.Parameter.Z_VELOCITY);
		private readonly ILocomotionModeCollection _locomotionModeCollection;

		public Walking(
			Animator animator,
			ILocomotionModeCollection locomotionModeCollection)
			: base(animator, Animator.StringToHash(Const.Animation.WALKING))
		{
			_locomotionModeCollection = locomotionModeCollection;
		}

		public void Update()
		{
			var maxSpeed = _locomotionModeCollection.Walking.MaxSpeed;
			var currentSpeed = _locomotionModeCollection.Walking.CurrentSpeed;
			HandleLocomotionAnimation(currentSpeed, maxSpeed);
		}

		private void HandleLocomotionAnimation(float currentSpeed, float maxSpeed)
		{
			var ratio = currentSpeed / maxSpeed;
			Animator.SetFloat(_zVelocity, ratio);
		}
	}
}