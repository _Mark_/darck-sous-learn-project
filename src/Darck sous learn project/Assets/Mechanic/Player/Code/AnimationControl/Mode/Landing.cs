﻿using UnityEngine;

namespace Mechanic.Player.AnimationControl.Mode
{
	public class Landing : Animation
	{
		public Landing(Animator animator)
			: base(animator, Animator.StringToHash(Const.Animation.LANDING)) { }
	}
}