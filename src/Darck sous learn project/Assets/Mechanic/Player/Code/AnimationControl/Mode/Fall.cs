﻿using UnityEngine;

namespace Mechanic.Player.AnimationControl.Mode
{
	public class Fall : Animation
	{
		public Fall(Animator animator)
			: base(
				animator,
				Animator.StringToHash(Const.Animation.FALLING)) { }
	}
}