﻿namespace Mechanic.Player.AnimationControl.Mode
{
	public interface IAnimationUpdate
	{
		void Update();
	}
}