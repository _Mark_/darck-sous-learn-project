﻿using UnityEngine;

namespace Mechanic.Player.AnimationControl.Mode
{
	public abstract class Animation : IAnimationLifeCycle
	{
		protected readonly int AnimationHash;
		protected readonly Animator Animator;

		protected Animation(Animator animator, int animationHash)
		{
			Animator = animator;
			AnimationHash = animationHash;
		}

		public bool IsActive =>
			Animator.GetBool(AnimationHash);

		public void Activate()
		{
			OnActivating();
			Animator.SetBool(AnimationHash, true);
		}

		public void Deactivate()
		{
			OnDeactivating();
			Animator.SetBool(AnimationHash, false);
		}

		protected virtual void OnActivating() {}
		protected virtual void OnDeactivating() {}
	}
}