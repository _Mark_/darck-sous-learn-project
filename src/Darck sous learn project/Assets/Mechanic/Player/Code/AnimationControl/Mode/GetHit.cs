﻿using Mechanic.Player.Locomotion;
using UnityEngine;

namespace Mechanic.Player.AnimationControl.Mode
{
	public class GetHit : Animation
	{
		private readonly Animator _animator;
		private readonly PlayerLocomotion _playerLocomotion;

		public GetHit(Animator animator, PlayerLocomotion playerLocomotion)
		: base(animator, Animator.StringToHash(Const.Animation.GET_HIT))
		{
			_animator = animator;
			_playerLocomotion = playerLocomotion;
		}

		private void OnPlayerLocomotionModeSwitched(Locomotion.EMode mode)
		{
			if (mode != Locomotion.EMode.GetHit) return;
			
			// _animator.CrossFade(_getHit, PlayerAnimationHandler.DEFAULT_TRANSITION_ANIMATION);
		}

		public void Activate()
		{
			throw new System.NotImplementedException();
		}

		public void Deactivate()
		{
			throw new System.NotImplementedException();
		}
	}
}