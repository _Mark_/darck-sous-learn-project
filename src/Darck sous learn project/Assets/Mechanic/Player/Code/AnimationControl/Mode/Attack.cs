﻿using UnityEngine;

namespace Mechanic.Player.AnimationControl.Mode
{
	public class Attack : Animation
	{
		private readonly int _swordAttackLeftHand = Animator.StringToHash(
			Const.Animation.Layer.ATTACK + '.' + Const.Animation.SWORD_ATTACK_LEFT_HAND);
		private readonly int _swordAttackRightHand = Animator.StringToHash(
			Const.Animation.Layer.ATTACK + '.' + Const.Animation.SWORD_ATTACK_RIGHT_HAND);
		private readonly Animator _animator;

		public Attack(Animator animator) : base(animator, 0)
		{
			_animator = animator;
		}

		public void LaunchSwordAttackLeftHand() { }

		public void LaunchSwordAttackRightHand() { }

		public void Activate()
		{
			throw new System.NotImplementedException();
		}

		public void Deactivate()
		{
			throw new System.NotImplementedException();
		}
	}
}