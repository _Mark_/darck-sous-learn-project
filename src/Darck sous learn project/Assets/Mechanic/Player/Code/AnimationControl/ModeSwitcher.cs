﻿using Mechanic.Player.AnimationControl.Mode;

namespace Mechanic.Player.AnimationControl
{
	public sealed class ModeSwitcher
	{
		private IAnimationLifeCycle _activeAnimation;
		
		private readonly AnimationCollection _animationCollection;

		public ModeSwitcher(AnimationCollection animationCollection) => 
			_animationCollection = animationCollection;

		public bool IsCanUpdateAnimation { get; private set; } = false;
		public EMode ActiveMode { get; private set; }
		public IAnimationUpdate ActiveUpdateAnimation { get; private set; }

		public void DeactivateActiveAnimation() =>
			_activeAnimation.Deactivate();

		public void ActivateMode(EMode mode)
		{
			_activeAnimation = _animationCollection.LifeCycleModes[mode];
			_activeAnimation.Activate();
			ActiveMode = mode;

			ChangeUpdateAnimation(mode);
		}

		private void ChangeUpdateAnimation(EMode mode)
		{
			if (_animationCollection.UpdatedAnimations.ContainsKey(mode))
			{
				IsCanUpdateAnimation = true;
				ActiveUpdateAnimation = _animationCollection.UpdatedAnimations[mode];
			}
			else
				IsCanUpdateAnimation = false;
		}
	}
}