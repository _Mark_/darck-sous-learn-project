﻿namespace Mechanic.Player.AnimationControl
{
	public interface IAnimationStateExitReader
	{
		public void AnimationExit();
	}
}