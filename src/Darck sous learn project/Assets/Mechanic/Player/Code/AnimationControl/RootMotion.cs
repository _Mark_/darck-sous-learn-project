﻿using UnityEngine;

namespace Mechanic.Player.AnimationControl
{
  public class RootMotion
  {
    private readonly Animator _animator;

    public RootMotion(Animator animator) =>
      _animator = animator;

    public bool Enabled
    {
      get => _animator.applyRootMotion;
      set
      {
        if (value == false)
          ResetDeltaPosition();
        _animator.applyRootMotion = value;
      }
    }

    public Vector3 DeltaPosition { get; private set; }

    public void AddToDeltaPosition(Vector3 value) =>
      DeltaPosition += value;


    public void ResetDeltaPosition() =>
      DeltaPosition = Vector3.zero;
  }
}