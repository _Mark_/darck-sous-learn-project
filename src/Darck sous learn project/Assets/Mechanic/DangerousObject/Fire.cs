﻿using System;
using System.Collections.Generic;
using Mechanic.Health;
using Mechanic.Interaction;
using UnityEngine;

namespace Mechanic.DangerousObject
{
	[RequireComponent(typeof(Damage), typeof(Trigger))]
	public class Fire : MonoBehaviour
	{
		private Damage _damage;
		private Trigger _trigger;
		private readonly List<IHealth> _healthList = new List<IHealth>();

		private void Awake()
		{
			_damage = GetComponent<Damage>();
			_trigger = GetComponent<Trigger>();
			_trigger.ColliderExited += OnTriggerColliderExit;
			_trigger.ColliderEntered += OnTriggerColliderEntered;
		}

		private void FixedUpdate()
		{
			foreach (var health in _healthList)
			{
				var damage = (int) (_damage.Value * Time.fixedDeltaTime);
				health.GiveDamage(damage);
			}
		}

		private void OnTriggerColliderEntered(Collider other)
		{
			if (other.TryGetComponent<IHealth>(out var health) == false)
				return;

			_healthList.Add(health);
		}

		private void OnTriggerColliderExit(Collider other)
		{
			if (other.TryGetComponent<IHealth>(out var health) == false)
				return;

			_healthList.Remove(health);
		}
	}
}