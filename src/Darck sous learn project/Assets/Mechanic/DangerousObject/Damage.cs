﻿using UnityEngine;

namespace Mechanic.DangerousObject
{
	public class Damage : MonoBehaviour
	{
		[SerializeField] private int _value = 1;
		public int Value => _value;

		public void Construct(int value)
		{
			_value = value;
		}
	}
}