﻿using System;

namespace Mechanic.Health
{
	public interface IHealth
	{
		event Action<Int32> GivenHeal;
		event Action<Int32> Changed;
		event Action<Int32> GivenDamage;
		
		int Max { get; }
		int Death { get; }
		int Value { get; }

		void GiveHeal(int heal);
		void GiveDamage(int damage);
	}
}