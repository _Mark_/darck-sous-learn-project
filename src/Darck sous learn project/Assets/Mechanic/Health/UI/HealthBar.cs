using Infrastructure;
using UnityEngine;
using UnityEngine.UI;

namespace Mechanic.Health.UI
{
	public class HealthBar : MonoBehaviour, IConstructor<Health>
	{
		[SerializeField] private Slider _slider;
		
		private Health _health;
		
		public void Construct(Health health)
		{
			_health = health;
			_slider.maxValue = _health.Max;
			_slider.minValue = _health.Death;
			_slider.value = _health.Value;
			_health.Changed += OnHealthChanged;
		}

		private void OnHealthChanged(int health)
		{
			_slider.value = health;
		}
	}
}