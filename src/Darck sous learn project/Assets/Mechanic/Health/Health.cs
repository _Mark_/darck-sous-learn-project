﻿using System;
using UnityEngine;
using int_heal = System.Int32;
using int_health = System.Int32;
using int_damage = System.Int32;

namespace Mechanic.Health
{
	public class Health : MonoBehaviour, IHealth
	{
		[SerializeField] private int _max;
		[SerializeField] private int _value;
		[SerializeField] private int _death;

		public event Action<int_heal> GivenHeal;
		public event Action<int_health> Changed;
		public event Action<int_damage> GivenDamage;

		public int Max => _max;
		public int Death => _death;
		public int Value => _value;

		public void GiveHeal(int heal)
		{
			_value += heal;
			OnGivenHeal(heal);
		}

		public void GiveDamage(int damage)
		{
			_value -= damage;
			OnGivenDamage(damage);
		}

		private void OnGivenHeal(int heal)
		{
			GivenHeal?.Invoke(heal);
			Changed?.Invoke(_value);
		}
		
		private void OnGivenDamage(int damage)
		{
			GivenDamage?.Invoke(damage);
			Changed?.Invoke(_value);
		}
	}
}