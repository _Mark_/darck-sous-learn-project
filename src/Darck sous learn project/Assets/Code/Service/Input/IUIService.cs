﻿namespace Service.Input
{
	public interface IUIService : IService
	{
		void Construct(Mechanic.Health.Health playerHealth);
	}
}