﻿using System;
using UnityEngine;

namespace Service.Input.Standard
{
  public class StandardInputService : MonoBehaviour, IInputService
  {
    private const float MAX = 1;
    private const float MIN = -1;

    private InputControl _inputControl;

    private Vector2 _camera;
    private Vector2 _movement;
    public event Action LmbPressed;
    public event Action RmbPressed;
    public event Action RollPressed;
    public event Action RunModePressed;

    public bool IsSprintPressed { get; private set; }

    public bool IsMovementButtonsPressed { get; set; }

    public Vector2 Camera
    {
      get => _camera;
      private set
      {
        var x = Mathf.Clamp(value.x, MIN, MAX);
        var y = Mathf.Clamp(value.y, MIN, MAX);
        _camera = new Vector2(x, y);
      }
    }

    public Vector2 Movement
    {
      get => _movement;
      private set
      {
        var x = Mathf.Clamp(value.x, MIN, MAX);
        var y = Mathf.Clamp(value.y, MIN, MAX);
        _movement = new Vector2(x, y);
      }
    }

    private void Awake() =>
      _inputControl = new InputControl();

    private void OnEnable() =>
      _inputControl.Enable();

    private void OnDisable() =>
      _inputControl.Disable();

    public void Construct()
    {
      _inputControl.PlayerAction.LMB.performed += context => LmbPressed?.Invoke();
      _inputControl.PlayerAction.RMB.performed += context => RmbPressed?.Invoke();
      _inputControl.PlayerMovement.Roll.performed += context => RollPressed?.Invoke();
      _inputControl.PlayerMovement.Camera.performed += context => Camera = context.ReadValue<Vector2>();
      _inputControl.PlayerMovement.RunMode.performed += context => RunModePressed?.Invoke();
      _inputControl.PlayerMovement.Movement.performed += context => Movement = context.ReadValue<Vector2>();
    }

    private void Update()
    {
      IsSprintPressed = _inputControl.PlayerMovement.Sprint.IsPressed();
      IsMovementButtonsPressed = _inputControl.PlayerMovement.Movement.IsPressed();
    }
  }
}