using System;
using UnityEngine;

namespace Service.Input
{
  public interface IInputService : IService
  {
    event Action LmbPressed;
    event Action RmbPressed;
    event Action RollPressed;
    event Action RunModePressed;
    bool IsSprintPressed { get; }
    bool IsMovementButtonsPressed { get; set; }
    Vector2 Camera { get; }
    Vector2 Movement { get; }
    void Construct();
  }
}
