﻿using System.Collections.Generic;

namespace Extension
{
	public static class List
	{
		public static List<T> AddNext<T>(this List<T> list, T element)
		{
			list.Add(element);
			return list;
		}
	}
}