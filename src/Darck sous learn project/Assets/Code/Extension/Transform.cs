﻿using UnityEngine;

namespace Extension
{
  public static class Transform
  {
    public static Vector3 Down(this UnityEngine.Transform transform) =>
      -transform.up;
  }
}