﻿namespace Infrastructure
{
  public interface IConstructor
  {
    void Construct();
  }
  
  public interface IConstructor<in T>
  {
    void Construct(T dependency);
  }
}