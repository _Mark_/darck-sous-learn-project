﻿using System.Linq;
using Extension;
using Mechanic.Health;
using Mechanic.Player.Locomotion;
using Service.Input;
using UnityEngine;

namespace Infrastructure
{
	public class SceneDependencyAllocator : MonoBehaviour,
		ISceneDependencyAllocator
	{
		[SerializeField] private Health _playerHealth;
		
		private IUIService _uiService;
		private IInputService _inputService;
		private PlayerInventory _playerInventory;

		public void Allocate()
		{
			FindServices();
			ConstructServices();

			Inject(_uiService);
			Inject(_inputService);
			Inject(_playerInventory);
			InjectLocalDependency();
			TransferDependencies();
			Construct();
			
			Destroy(gameObject);
		}

		private void FindServices()
		{
			_uiService = this.FindObjectOfType<IUIService>(true);
			_inputService = this.FindObjectOfType<IInputService>(true);
			_playerInventory = FindObjectOfType<PlayerInventory>(true);
		}

		private void ConstructServices()
		{
			_uiService.Construct(_playerHealth);
			_inputService.Construct();
		}

		private void Inject<T>(T dependency)
		{
			this.FindObjectsOfType<IConstructor<T>>(true)
				.ToList()
				.ForEach(constructor => constructor.Construct(dependency));
		}

		private void InjectLocalDependency()
		{
			this.FindObjectsOfType<ILocalDependencyAllocator>(true)
				.ToList()
				.ForEach(constructor => constructor.InjectDependency());
		}

		private void TransferDependencies()
		{
			this.FindObjectsOfType<IDependencyCollector>(true)
				.ToList()
				.ForEach(dependencyCollector =>
					dependencyCollector.TransferDependencies());
		}

		private void Construct()
		{
			this.FindObjectsOfType<IConstructor>(true)
				.ToList()
				.ForEach(constructor => constructor.Construct());
		}
	}
}