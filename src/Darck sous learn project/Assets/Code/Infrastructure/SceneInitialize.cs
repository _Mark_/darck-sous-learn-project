﻿using Extension;
using UnityEngine;

namespace Infrastructure
{
	public class SceneInitialize : MonoBehaviour, ISceneInitializer
	{
		public void Initialize()
		{
			var initializers = this.FindObjectsOfType<IInitializer>();

			foreach (var initializer in initializers) 
				initializer.Initialize();
			
			Destroy(gameObject);
		}
	}
}