﻿using Extension;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Infrastructure
{
	public class SceneGameBootstrapper : MonoBehaviour
	{
		private ISceneInitializer _sceneInitializer;
		private ISceneDependencyAllocator _sceneDependencyAllocator;

		private void Awake()
		{
			FindSceneDependencyAllocator();
			FindGameInitializer();
		}

		private void Start()
		{
			_sceneDependencyAllocator.Allocate();
			_sceneInitializer.Initialize();
			
			Destroy(gameObject);
		}

		private void FindSceneDependencyAllocator()
		{
			var sceneDependencyAllocators = this.FindObjectsOfType<ISceneDependencyAllocator>();
			var allocatorsQuantity = sceneDependencyAllocators.Length;

			if (allocatorsQuantity == 0)
			{
				Debug.LogError($"Can not to find \"{nameof(ISceneDependencyAllocator)}\"!");
				return;
			}

			if (allocatorsQuantity > 1)
			{
				Debug.LogError($"The scene have \"{nameof(ISceneDependencyAllocator)}\" " +
				               $"more than one!");
				return;
			}

			_sceneDependencyAllocator = sceneDependencyAllocators[0];
		}

		private void FindGameInitializer()
		{
			var sceneInitializers = this.FindObjectsOfType<ISceneInitializer>();
			var initializersQuantity = sceneInitializers.Length;

			if (initializersQuantity == 0)
			{
				Debug.LogError($"Can not to find \"{nameof(ISceneInitializer)}\"!");
				return;
			}

			if (initializersQuantity > 1)
			{
				Debug.LogError($"The scene have \"{nameof(ISceneInitializer)}\" " +
				               $"more than one!");
				return;
			}

			_sceneInitializer = sceneInitializers[0];
		}
	}
}