﻿namespace Infrastructure.DI
{
	public interface IProvider<out T>
	{
		T Provide();
	}
}