﻿using System;
using System.Collections.Generic;
using IProvider = Infrastructure.DI.IProvider<object>;

namespace Infrastructure.DI
{
	public class ContextWithId
	{
		public readonly Dictionary<ValueTuple<Type, string>, ELifeCycle> LifeCycles;
		public readonly Dictionary<Tuple<Type, string>, IProvider> Singleton;
		public readonly Dictionary<Type, IProvider> Scope;
		public readonly Dictionary<Type, IProvider> Transient;
	}
}