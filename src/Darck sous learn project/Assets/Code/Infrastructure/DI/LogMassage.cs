﻿namespace Infrastructure.DI
{
	public static class LogMassage
	{
		public static class DependencyProvider
		{
			public static class Error
			{
				public static string HasNotDependency<T>() =>
					$"Container has not the dependency: \"{typeof(T)}\"!";
				
				public static string IncorrectLifeCycle(ELifeCycle lifeCycle) =>
					$"Incorrect life cycle: \"{lifeCycle}\"!";
			}
		}

		public static class CollectionContainer
		{
			public static class Error
			{
				public static string IncorrectLifeCycle(ELifeCycle lifeCycle) =>
					$"Incorrect life cycle: \"{lifeCycle}\"!";
			}
		}
	}
}