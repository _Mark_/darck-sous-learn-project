﻿namespace Infrastructure.DI
{
	public enum ELifeCycle
	{
		Singleton,
		Scope,
		Transient,
	}
}