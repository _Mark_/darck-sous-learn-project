﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ErrorMessage = Infrastructure.DI.LogMassage.DependencyProvider.Error;

namespace Infrastructure.DI
{
	public class DependencyProvider : IDependencyProvider
	{
		private readonly Context _context;

		public DependencyProvider(Context context)
		{
			_context = context;
		}

		public T Get<T>()
		{
			if (_context.IsContains<T>() == false)
			{
				Debug.LogError(ErrorMessage.HasNotDependency<T>());
				return default;
			}

			var result = DependencyObject<T>();
			return result;
		}

		private T DependencyObject<T>()
		{
			T result = default;
			var type = typeof(T);
			var lifeCycle = _context.LifeCycleFor<T>();
			var provider = _context.ProviderFor<T>(lifeCycle);

			switch (lifeCycle)
			{
				case ELifeCycle.Singleton:
					var singleton = _context.ObjectsContainer.Singleton;
					result = FromCollection<T>(type, provider, singleton);
					break;

				case ELifeCycle.Scope:
					var scope = _context.ObjectsContainer.Scope;
					result = FromCollection<T>(type, provider, scope);
					break;

				case ELifeCycle.Transient:
					result = (T)provider.Provide();
					break;

				default:
					Debug.LogError(ErrorMessage.IncorrectLifeCycle(lifeCycle));
					return result;
			}

			return result;
		}

		private T FromCollection<T>(
			Type type,
			IProvider<object> provider,
			Dictionary<Type, object> collection)
		{
			if (_context.Scope.ContainsKey(type) == false)
			{
				var obj = provider.Provide();
				collection.Add(type, obj);
			}

			return (T)collection[type];
		}
	}
}