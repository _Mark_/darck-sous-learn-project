﻿using System;
using System.Collections.Generic;

namespace Infrastructure.DI
{
	public class ProvidersContainer
	{
		public readonly Dictionary<Type, ELifeCycle> LifeCycles;
		public readonly Dictionary<Type, IProvider<object>> Singleton;
		public readonly Dictionary<Type, IProvider<object>> Scope;
		public readonly Dictionary<Type, IProvider<object>> Transient;

		public ProvidersContainer(
			Dictionary<Type, ELifeCycle> lifeCycles,
			Dictionary<Type, IProvider<object>> singleton,
			Dictionary<Type, IProvider<object>> scope,
			Dictionary<Type, IProvider<object>> transient)
		{
			LifeCycles = lifeCycles;
			Singleton = singleton;
			Scope = scope;
			Transient = transient;
		}
	}
}