﻿using Infrastructure.DI.Constructor;

namespace Infrastructure.DI
{
	public class DependencyCollection : ICollection
	{
		private readonly Cleaner _cleaner;
		private readonly Context _container;
		private readonly CollectionConstructor _constructor;

		public DependencyCollection()
		{
			_container = new Context();
			
			_cleaner = new Cleaner(_container);
			_constructor = new CollectionConstructor(_container);
		}

		public ICollectionConstructor Add<T>() =>
			_constructor.Bind<T>();

		public ICollection Remove<T>()
		{
			_cleaner.Remove<T>();
			return this;
		}

		public bool IsContains<T>() =>
			_container.IsContains<T>();

		public IDependencyProvider BuildDependencyProvider()
		{
			var collectionContainer = _container.BuildClone();
			var provider = new DependencyProvider(collectionContainer);
			return provider;
		}
	}
}