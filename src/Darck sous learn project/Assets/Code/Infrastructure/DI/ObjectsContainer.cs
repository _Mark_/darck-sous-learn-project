﻿using System;
using System.Collections.Generic;

namespace Infrastructure.DI
{
	public class ObjectsContainer
	{
		public readonly Dictionary<Type, object> Scope;
		public readonly Dictionary<Type, object> Singleton;

		public ObjectsContainer(Dictionary<Type, object> singleton)
		{
			Scope = new Dictionary<Type, object>();
			Singleton = singleton;
		}
	}
}