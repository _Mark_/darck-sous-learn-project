﻿using System;
using System.Collections.Generic;
using Infrastructure.DI.Provider;
using UnityEngine;

namespace Infrastructure.DI.Constructor
{
	public class LifeCycle
	{
		private readonly Context _container;
		private readonly CollectionConstructor _constructor;

		public LifeCycle(
			Context container,
			CollectionConstructor constructor)
		{
			_container = container;
			_constructor = constructor;
		}

		public void As(ELifeCycle lifeCycle)
		{
			Dictionary<Type, IProvider<object>> collection;

			switch (lifeCycle)
			{
				case ELifeCycle.Scope:
					collection = _container.Scope;
					break;

				case ELifeCycle.Singleton:
					collection = _container.Singleton;
					break;

				case ELifeCycle.Transient:
					collection = _container.Transient;
					break;

				default:
					Debug.LogError($"Incorrect life cycle type: \"{lifeCycle}\"!");
					return;
			}

			CreateDependency(lifeCycle, collection);
		}

		private void CreateDependency(
			ELifeCycle typeLifeCycle,
			Dictionary<Type, IProvider<object>> collection)
		{
			if (IsProviderTypeCorrect() == false)
				return;

			var typeKey = _constructor.TypeKey;
			AddToCollection(typeKey, collection);
			SetLifeCycle(typeKey, typeLifeCycle);
		}

		private bool IsProviderTypeCorrect()
		{
			var providerType = _constructor.ProviderType;
			switch (providerType)
			{
				case EProviderType.Method:
				case EProviderType.Factory:
				case EProviderType.Instance:
					break;
				default:
					Debug.LogError($"Incorrect provider type: \"{providerType}\"");
					return false;
			}

			return true;
		}

		private void AddToCollection(
			Type typeKey,
			Dictionary<Type, IProvider<object>> collection)
		{
			var provider = _constructor.Provider;
			collection.Add(typeKey, provider);
		}

		private void SetLifeCycle(Type typeKey, ELifeCycle lifeCycle)
		{
			_container.LifeCycles.Add(typeKey, lifeCycle);
		}
	}
}