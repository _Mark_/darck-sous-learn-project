﻿using System;
using Infrastructure.DI.Provider;

namespace Infrastructure.DI.Constructor
{
	public class Provider
	{
		private readonly LifeCycle _lifeCycle;
		private readonly CollectionConstructor _constructor;

		public Provider(CollectionConstructor constructor, LifeCycle lifeCycle)
		{
			_lifeCycle = lifeCycle;
			_constructor = constructor;
		}

		public LifeCycle Instance(object instance)
		{
			_constructor.Provider = new ByInstance<object>(instance);
			_constructor.ProviderType = EProviderType.Instance;
			return _lifeCycle;
		}

		public LifeCycle FromMethod(Func<object> method)
		{
			_constructor.Provider = new ByMethod<object>(method);
			_constructor.ProviderType = EProviderType.Method;
			return _lifeCycle;
		}

		public LifeCycle FromFactory(IRealizationFactory<object> factory)
		{
			_constructor.Provider = new ByFactoryClass<object>(factory);
			_constructor.ProviderType = EProviderType.Factory;
			return _lifeCycle;
		}
	}
}