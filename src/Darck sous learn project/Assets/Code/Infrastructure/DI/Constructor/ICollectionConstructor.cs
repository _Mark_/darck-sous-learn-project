﻿using System;

namespace Infrastructure.DI.Constructor
{
	public interface ICollectionConstructor
	{
		Provider WithKey(string key);
		LifeCycle Instance(object instance);
		LifeCycle FromMethod(Func<object> method);
		LifeCycle FromFactory(IRealizationFactory<object> factory);
	}
}