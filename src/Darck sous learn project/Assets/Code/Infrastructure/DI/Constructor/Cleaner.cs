﻿using IProvider = Infrastructure.DI.IProvider<object>;

namespace Infrastructure.DI.Constructor
{
	public class Cleaner
	{
		private readonly Context _container;

		public Cleaner(Context container)
		{
			_container = container;
		}

		public Cleaner Remove<T>()
		{
			if (_container.IsContains<T>() == false)
				return this;

			var type = typeof(T);
			_container.LifeCycles.Remove(type);
			_container.Scope.Remove(type);
			_container.Transient.Remove(type);
			_container.Singleton.Remove(type);

			return this;
		}
	}
}