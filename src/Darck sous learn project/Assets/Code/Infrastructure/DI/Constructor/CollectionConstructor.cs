﻿using System;
using Infrastructure.DI.Provider;
using UnityEngine.InputSystem.Controls;

namespace Infrastructure.DI.Constructor
{
	public class CollectionConstructor : ICollectionConstructor
	{
		private readonly Provider _provider;

		public CollectionConstructor(Context container)
		{
			var lifeCycle = new LifeCycle(container, this);
			_provider = new Provider(this, lifeCycle);
		}

		public Type TypeKey { get; private set; }
		public bool IsWithKey { get; private set; }
		public string Key { get; private set; }
		public IProvider<object> Provider { get; set; }
		public EProviderType ProviderType { get; set; }

		public CollectionConstructor Bind<T>()
		{
			Key = null;
			TypeKey = typeof(T);
			IsWithKey = false;
			
			return this;
		}

		public Provider WithKey(string key)
		{
			Key = key;
			IsWithKey = true;
			
			return _provider;
		}

		public LifeCycle Instance(object instance) =>
			_provider.Instance(instance);

		public LifeCycle FromMethod(Func<object> method) =>
			_provider.FromMethod(method);

		public LifeCycle FromFactory(IRealizationFactory<object> factory) =>
			_provider.FromFactory(factory);
	}
}