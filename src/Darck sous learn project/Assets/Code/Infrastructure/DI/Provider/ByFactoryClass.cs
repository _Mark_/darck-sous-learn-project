﻿using UnityEngine;

namespace Infrastructure.DI.Provider
{
	public class ByFactoryClass<TRealization> : IProvider<TRealization>
	{
		private readonly IRealizationFactory<TRealization> _factory;

		public ByFactoryClass(IRealizationFactory<TRealization> factory)
		{
			if (factory == null)
			{
				Debug.LogError($"\"{nameof(factory)}\" is null!");
				return;
			}

			_factory = factory;
		}

		public TRealization Provide() =>
			_factory.Create();
	}
}