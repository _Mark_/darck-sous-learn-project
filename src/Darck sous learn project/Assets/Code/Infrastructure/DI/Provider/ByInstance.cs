﻿using UnityEngine;

namespace Infrastructure.DI.Provider
{
	public class ByInstance<TInstance> : IProvider<TInstance>
	{
		private readonly TInstance _instance;

		public ByInstance(TInstance instance)
		{
			if (instance == null)
			{
				Debug.LogError($"\"{nameof(instance)}\" is null!");
				return;
			}

			_instance = instance;
		}

		public TInstance Provide() =>
			_instance;
	}
}