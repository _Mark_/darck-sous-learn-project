﻿using System;
using UnityEngine;

namespace Infrastructure.DI.Provider
{
	public class ByMethod<TRealization> : IProvider<TRealization>
	{
		private readonly Func<TRealization> _provide;

		public ByMethod(Func<TRealization> provide)
		{
			if (provide == null)
			{
				Debug.LogError($"\"{nameof(provide)}\" is null!");
				return;
			}

			_provide = provide;
		}

		public TRealization Provide() =>
			_provide.Invoke();
	}
}