﻿namespace Infrastructure.DI.Provider
{
	public enum EProviderType
	{
		Method,
		Factory,
		Instance,
	}
}