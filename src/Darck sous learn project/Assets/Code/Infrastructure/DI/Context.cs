﻿using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;
using IProvider = Infrastructure.DI.IProvider<object>;
using ErrorMessage = Infrastructure.DI.LogMassage.CollectionContainer.Error;

namespace Infrastructure.DI
{
	public class Context
	{
		public readonly Dictionary<Type, ELifeCycle> LifeCycles;
		public readonly Dictionary<Type, IProvider> Singleton;
		public readonly Dictionary<Type, IProvider> Scope;
		public readonly Dictionary<Type, IProvider> Transient;

		public Context()
		{
			Scope = new Dictionary<Type, IProvider>();
			Singleton = new Dictionary<Type, IProvider>();
			Transient = new Dictionary<Type, IProvider>();
			LifeCycles = new Dictionary<Type, ELifeCycle>();
			ObjectsContainer = new ObjectsContainer(new Dictionary<Type, object>());
		}

		private Context(ProvidersContainer providers, ObjectsContainer objects)
		{
			Scope = providers.Scope;
			Singleton = providers.Singleton;
			Transient = providers.Transient;
			LifeCycles = providers.LifeCycles;
			ObjectsContainer = objects;
		}

		public ObjectsContainer ObjectsContainer { get; }

		public bool IsContains<T>()
		{
			var type = typeof(T);

			if (Singleton.ContainsKey(type))
				return true;
			if (Scope.ContainsKey(type))
				return true;
			if (Transient.ContainsKey(type))
				return true;

			return false;
		}

		public ELifeCycle LifeCycleFor<T>()
		{
			var type = typeof(T);
			var lifeCycle = LifeCycles[type];
			return lifeCycle;
		}

		public IProvider ProviderFor<T>(ELifeCycle lifeCycle)
		{
			var type = typeof(T);
			IProvider result = null;

			switch (lifeCycle)
			{
				case ELifeCycle.Singleton:
					result = Singleton[type];
					break;

				case ELifeCycle.Scope:
					result = Scope[type];
					break;

				case ELifeCycle.Transient:
					result = Transient[type];
					break;

				default:
					Debug.LogError(ErrorMessage.IncorrectLifeCycle(lifeCycle));
					break;
			}

			return result;
		}

		public Context BuildClone()
		{
			var objects = new ObjectsContainer(ObjectsContainer.Singleton);
			var providers = BuildProvidersContainer();
			
			var copy = new Context(providers, objects);

			return copy;
		}

		private ProvidersContainer BuildProvidersContainer()
		{
			var lifeCycles = new Dictionary<Type, ELifeCycle>(LifeCycles);
			var scope = new Dictionary<Type, IProvider<object>>(Scope);
			var transient = new Dictionary<Type, IProvider<object>>(Transient);

			var providers = new ProvidersContainer(
				lifeCycles,
				singleton: Singleton,
				scope: scope,
				transient: transient);
			
			return providers;
		}
	}
}