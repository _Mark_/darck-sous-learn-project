﻿using Infrastructure.DI.Constructor;

namespace Infrastructure.DI
{
	public interface ICollection
	{
		ICollectionConstructor Add<T>();
		ICollection Remove<T>();
		bool IsContains<T>();
		IDependencyProvider BuildDependencyProvider();
	}
}