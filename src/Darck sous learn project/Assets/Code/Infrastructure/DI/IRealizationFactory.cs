﻿namespace Infrastructure.DI
{
	public interface IRealizationFactory<out TInstance>
	{
		TInstance Create();
	}
}