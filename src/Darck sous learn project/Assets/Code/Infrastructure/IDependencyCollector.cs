﻿namespace Infrastructure
{
	public interface IDependencyCollector
	{
		void TransferDependencies();
	}
}