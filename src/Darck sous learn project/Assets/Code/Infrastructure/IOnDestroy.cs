﻿namespace Infrastructure
{
	public interface IOnDestroy
	{
		void OnDestroy();
	}
}