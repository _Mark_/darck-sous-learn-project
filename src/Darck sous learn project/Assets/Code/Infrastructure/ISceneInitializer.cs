﻿namespace Infrastructure
{
	public interface ISceneInitializer
	{
		void Initialize();
	}
}