﻿namespace Infrastructure
{
  public static class Const
  {
    public static class CreateAssetMenu
    {
      public static class MenuName
      {
        public const string SCRIPTABLE_OBJECT = "Scriptable object";
        public const string ITEM_WEAPON = SCRIPTABLE_OBJECT + "/Item/Weapon";
      }
    }
  }
}