﻿namespace Infrastructure
{
	public interface ISceneDependencyAllocator
	{
		void Allocate();
	}
}