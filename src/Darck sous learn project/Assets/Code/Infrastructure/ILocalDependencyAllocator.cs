﻿namespace Infrastructure
{
  public interface ILocalDependencyAllocator
  {
    void InjectDependency();
  }
}